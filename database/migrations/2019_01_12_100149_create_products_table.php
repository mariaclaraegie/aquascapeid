<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->foreign('subcategory_id')->references('id')->on('subcategorys');
            $table->integer('subcategory_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('decs')->nullable();
            $table->string('easydesc')->nullable();
            $table->string('mediumdesc')->nullable();
            $table->string('advancedesc')->nullable();
            $table->string('price')->nullable();
            $table->string('type')->nullable();
            $table->string('origin')->nullable();
            $table->string('growth')->nullable();
            $table->string('height')->nullable();
            $table->string('lightdemand')->nullable();
            $table->string('co')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
