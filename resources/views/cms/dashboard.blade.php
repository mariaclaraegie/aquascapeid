@extends('layouts.cms')

@section('content')
<div class="app-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-2">
        <h3 class="content-header-title mb-0">Dashboard</h3>
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Hello {{Auth::user()->name}}! Welcome Back
                </li>
            </ol>
            </div>
        </div>
        </div>
        <!-- Columns section start -->
<section id="columns">
	<div class="row">
		<div class="col-xs-12 mt-3 mb-1">
			<p>You can edit Your Website here.</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 mt-1">
			<div class="">
				<div class="card">
					<div class="card-body">
						<img class="card-img-top img-fluid" style="max-height: 130px; width: 100%; object-fit: cover;" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxATEhMTExMVFhUXGBYYFRYWGR0aGBUXFRcXFh0YFR0YHSggGBolGxYaITEiJSsrLi4uFx8zODMsNygtLisBCgoKDg0OGxAQGzIlICUtLS8tLS0wLSstLS0tLy0tLS0vLS0tLS0tLS0tLS0tLS0tNS0tNS0tLS0tLS0tLS0tLf/AABEIALABHgMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABQYDBAcCAf/EAEwQAAEDAwICBQYJCAgGAwEAAAEAAhEDBCESMQVBBhMiUWEHMnGBkZIUI0JiobHB0uEVFjNSstHT8CQ0Q1NUY3KiJXN0gpPxNUSzF//EABkBAQADAQEAAAAAAAAAAAAAAAABAgMEBf/EACcRAQACAgAGAgICAwAAAAAAAAABAgMRBBITFCExQVFhkSLwUoHB/9oADAMBAAIRAxEAPwDuKIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICLFWuGNa5znNa1gJe5xADQBJLidhGcr7VrMa0vc4NaBJcSA0DvJOAEGRF8LgNz/O680arXNDmuDmuALXNMgg5BBG4Qe0ReHVWggFwBOwJyfQg9osNvd0qnmPY+A0nS4GA4S04OxGR3r3Rqte0Oa4OacgtMgjwI3Qe0ReKdVrvNcD6DP1IPaLCbumHaC9mqQNOoapILgI3nS0mO4ErMgIi8UqzXea4OjeCD9SD2iIgIvjnACSYHeV9QEREBERAREQEREBERAREQEREBERAREQVHiVrdD8qClSqa67fiKjHMb2xatYM6w5h1tgH0HxUBfdZcfC6YqVnuqC8o0Kevs1HNDA4OBqQzQ6m9oJY0ZnUZk9NXhtJoJcAJO5jJ9JSPBKoVOD3AuWuLaz6VO4caU1S4tpvtGNLpdU1FvXapBzk4grQ4Lwm/p0KFMMrsaylatrUzWBc9zH/GCgRUPVjR3FsiANl0FEFKoWPENVEnr9DajzUb13adRNwXUWTry9jYLzPaZ2JedpDifD6guxWo0S5z+qbVc8UzT6thcdTCXdYx7dRiBBJGNyLKiChcJ4Vf06dux7awpsbbCpTp1Q10MtXscGFrxAFbSSARMTkbuCcHv2fBmvNamxlK2ADXBwY5jn9ayqBVDXagWgu0vxtBCvqIKozg9weGCk41HV3Mpmq2pVLy8gtNSnqc4gBzQW4MdpYKzL3VUbTtdFFxo6dOmlVaA2sXfoa7dYBFJo7TfPccgQrkiDnlDgvET1D3MIrDqHOe5zDpqNsLikXOhxmKr2gxMzORJW9Y8JvCbcVH3IZ1odWaapbpAtqjfObXc57TV0SJiRIESrqiCp9EhXe+5L6rn06JNvROsuFQNc55qOJPafD2UyTmaTu8qHsuB3rbe3FGgbetRs3Unv1MDqlQikAxuhxmC15DnEQSI3JHQmMAEAADkBgL0go11wm/Lew+5gUrwsHW6C2s5tDqWn45xeNQqEanECSDAhZ3cOvGvg/CH0NYJa2uesdNsB2XGoHBoqzLdQyQdlckQVjiXCa9XhrKVZoq3AZSJnSfjWlsuBMDUM5VnREBERAREQEREBERAREQEREBERAREQEREFdv6l6KpdTa5zWuJaOyAQKb+yQCCQXaYdOJ2ws4ur8n9EyJIzuMtAPn9oCSeU6TgYS64/oqOZ1RIa4AukjBDiSBp3Gn0HU3OVqVeOVuor1AwhwcRTBB7HxIqAP7OTrlsAecdM80GS/vbw1KjaTA4U34gb/FNdpdLxqBLyOUaRusrru+1SKYDe0I0yfObB8/MNk8tRxAXgdISJBpuMPLZzt1jmzhsAaWyJIlYKHH64DC6k550AOAES/sEukgafOLdP6wAncoNp1e9fSM09D9dLDc9kkaxOsbZyDttnbwb3iEfoWTAPm8y1piOs7y5u+NE51Y+3XGqrK7mmnLGkgBslzsUC1w7O81HiJjsnOFkqcecGU3dS6XiodMzHVzGQ0g6oEc+0MboPtS5vDTZFMipqeTGkNIY46Wu1EkB7YyMjwXg3t9j4lu4zp5a41R1mDpzp9c8l8pcfeXfoTpLgGwTJGotJ83vEjvAORC83XHXippDMMe5rokhwDKhEkN7BlrTzw7KDxTvL+Q40TJDAW40gSXOIGvBE6ZzMDEbZru7vWvqCnSDmy7TImRpER8YIzjbxX08YqupPc2lpI6oCSf7XQf1dw14PPOFhHSN5DYoOBMHtk4BqBp1aWn5OonuLSCOZDzccXvWv0ii0EmpoDhl4a1ujZ0AlxIJzAEmBlZzXvXU6gLA100dOkbhzh1m7gcCe6J5lY/zjfmLdxgEuGoiIFLvbyNWCeXVvPKFOWtfW0OgidwdweYPoKCu3NbiBDopkEsA7J80gPdLRr87XpbMwRBiMLetL271fG0g1kx2RJy5rW51Y84k4OG+pTKIIC7u+IS4MpNiaul25ho7EDVkk98D7fd1VvBWlrdTARAiAWu6oOnt+cDrIMYjnKnEQV+5ub91uPig2o5tXUGHLCGnRpJcADPp2WGpccQDpbS2FQAOPZdqqthzu3OGaobHrzCsyIIK54lcsptc5rA51YtjSTppguyId2nECRG8xC1TcX5c1/VOBAyyQGYFbcajnNPbmN1Z0QQ1ndXhe0VG02g7gAyA0MJdOvLSS5owCDGFiq3N/j4ppIc7zdnDS8B3n7amg6Tye3IIMTyIKza1L9rmS1zgDBBG7CWgk/Gb+cRMxHOVvtvLvrdPUxT1xqMSWZE+f4A+g7ThS6IIB/GbjU5opQQ9zWNIMv7FNzczA7TzJEwBsYJXl3Er3WWNpsJDQ6C2Jk1ACfjOzOlojJGqfBWFEELeXdy6i3q6Z1kFtTGktOjdnbgdo952PNY6d3f4HVNxMkiNQD3CR28EtDcfOmcQp5EERUvq7RQ1NaHPcA9gBJALmt7JBjAMk92YABI0hfXjqmprAWNe9uMBwFRzCCC/cNa1wd84iO+yIgr4vOIZPVN2ENgb9on+079Ld9u181bBvLvS34kajV0nuFLHa86Zj6vQTMIgibW5upoNezdnxz4A0v0nYBx+UAI+cIO8aFK74iAJpAugaiW4nTRBgdZgSapx+p4qyogjOGXldznNrUgyZLIz2Wx52dzqBx4jlmTREBFGcRvKjHw2IgHI8SsA4hW+b7PxWkY5lnOSITSKG+H1vm+z8V8+HV/m+z8Vbo2V69U0ihfh9b5vs/FfG8SqnYtPoE/anQsr3NE2vFKk1s6WgSS4wIlx3Jjc+Ki/htbw9n4rGL6vqiWx/p/FR0bJ7iiZZTAmABJJMCJJ3J8V6Wi68IbLoUbW6RgbBK4b29I7miwIqLxDp02m4NNSk0nYO3P0qH4Z5UTVvaFq3S7rKjWFwEAA92ZlTfDNI8zC9c0W9OpIue8V6S8QbcVmUzS0te4NBYSYB5nVkr3R6R8RO4p+4fvLPS/N+F/RUpnSG856Pd/Fb1rxyufODfUPxUaTtZ0Ve/LFaeXu/isw4lVicez8U0bTaKC/KVfcafZ+K1avGrgfq+z8U0bWdFT39Irkfqe7+KwP6U3PIM90/vU8so5oXdFQqvS265aPd/FbnRvpFcVqr21CyBTc4Q2DILfHbKjSdwuKLiVDyncTIBmh/4z99bLPKRxHn1P8A4z99b9tdh3NHZEXI2+ULiHfR9w/eXsdP7/8AyvcP3k7a53NHWUXJx0/v+fVe4fvLMzp5fHnS9w/eTtrnc0dSRc1pdNb0/wB37h+8vtbpnejbq/cP3lXoWW69XSUXMPz3v5x1XuH7yyP6aXwG9Kf9B+8k4LQRnrLpaLlrunN//le4fvK1dBeOV7ptY1tMsc0N0iMEE5yVS2OYja1ckWnUNrjNcCqAf1R9ZXmjXb3qv9OWVDdDS4AdW39p6h2064H6QfSkVn/JaZr8wu11duaJawv9B+xatxxlzRig4nnJj2YkqqPfWHyx9K1at7WB/SNA8Rz5cx7FvX86n9ua0VmfmP0sNzxbrCCbZ8+l0eoYW/ZurvMgmm3kOraPtlcz422/rFobdGmzno1MM7TIMnv3/etrglDidJw03xeyMtqtL/HBLtX0q9r29RX/AKpFMfzZ1htSG9rMc8e3dRvGOkVC2aH1CO1OgfrRHMYG4ye9UHpJV4hUpPbFJzCBq7b5O3IDv5SRyMqgv4NcOOqoTPLmMbAYwOQ2Cwta0fC8Y8c+pdG6SeVGlllFgqAsBEgt0v1R2jORAOw5jK55xDphdVQ4FwaH47OABM4B27p9KyU+itR8hrmjAOTJJPKATleavQ2uyMOqGPk4AjxdE+xZde0eNtq8PHwrbyTInM5OP5hTvk7o/wDE7Ez/AG7OXpC2G9D6x7WkgjcEO38MZ9SluhvRm6ZxGzeWQxtVhcZ2A/nbdZ8zaKzEelt/LDXX97Sc4B7K9QNG0tkRvuZPJWGxu53hcZ6ZPji19uCK7yD3EQQVZ+BdMQ6G1p1ROtoie4Fvfvt7Fbcb1Kkzp1BlSn3D2Ldty09ygLHU5rXAiCARPcVJUqLwd2/T+5W0bTDaQ7lkLBzWtRquAyB6j+C9PeT3Kuk7fSGjYLXrNB5BfXh/JaV1espuDX1KbXO2DnAE+oqysyVaDP1VoXFkD5qirvpkztNYwlwLgCcDGA84OCeXcCVz/ivSaq/tPqlzgCJGAc50gYAPM845hVnJ9eUc0LRxLj9pSc5r6kOa7SQATJjMRuOU94IUh0A4/b1rx9KkS4/BqjyYgAB1MQZzPaXHLmu13fPpV28iRH5RqY/+pW//AEoq25+UxCNsqJLNWIETnv8ADcqSo2LjgESBME/UdlntBiIGR3fuW1TtzMjHo/8Aa1niZZxw9UfXtnsMEd+RzhZaFMkDIEkjP29ymqVEFulwkeHf3nOV8ZZDk0EeLjMepV7ydaT2sb20GUGnXDgdO0AnUJjH1r1StSQCOeY5xvjvwpb8ns2DdOZkHn6lnp2YxMy0ktcHOmTEz7FXvJ0ntaoqlLW6tJ3AyRMnYRvncLc4iIY3GZAjYyfSt51lTee2wEZ2nw37QnZZ2cJe8VZYHhzaYEvyHM+WQ7DohoDcSJk8llPG+fLSOEjXhDWls6NRAHcJEkd8TsvNcNaJc4fz3LZubOtDS5o1DzxTJJf75AEZ279lH3drVOnS0jEO6wzOZ5SN848FavFxZE8LpqV6wJxt3q+eSrzLj/Uz9kqiVLCqIh0CIxJJkZ3iFffJbRLW3Mzl7N/9JV5z1t4hFcM1ncsPTmpFyP8Alt/aeq8++23Uj5SLjTdgf5TP2qiqhu1vSkacOXNaLzCSrXJKguK8FFZ7XuqP7OzeTfFuN1tG8WjX4g89/q2V+WutM6ZbzPh9p2DKdU1A5xcRGTj2CFOWVaMzhU67u/nR9hW5wriZbDSC6ef87pEw0yRaa72vFeqNMEqOqgEKCv8AibiDBj+eSr9zxN0fKk+lRa0RDPFitk+dLlWtQZWJoqM81xHrVN/KdRoPacD+5bVjx2sWgvfOrzQQJj1ZXJkyY9bl34uEzb1WYXCjxWoPOAP0H6FO9HOIMdc0BkEvb9a5uOLPIn6fWpnoTxDVf2oneqwfSuWMkTPiHfHC3iN2lXenTgOKXxmD11SPaFqBhEHlAIPLwafBbXTx3/E74Gc13xC0LN405zJjB5H04P4la3hx2X3on0uFGi9r5Ok4yN3Zgk7jP0Le/PmvrAJY0RPmOIHLefWud8Muclr/ADZyDmAMgbcsmPBZCdQ2bAkHMCBG0Y9foU05t8rOd7dGvOn9SnDAGueYOoAwBnkCZ5Z5dy92vlAuHH9HTiRmDkR3h0DunMLmT7sCIyBt6fQvVtdF5DScenAHqwFrNLaR/J0riXlCrucOoa1rWmXHBnG0ux9HdyVWuruo91Sq+XF5Di4gQSO6GxHo7lFB9OWtBJHOcY39n70deHLw7IHp9EdwH2rK0Sidz7Lp5DJBIJkNmACOZGFB3Dwcd30+iFlv7pz3yeQx4fu5LQfK1pXUNawNO5JV/wDIef8AiNUd1pW+l9Fc+JiNjCvvkMM8Rrf9JW/bopLRkpcRoNAmo3blLvqlblPi9v8A3n+137lXrTgVu4Nh9UYGDjlz1NBCz3PDLVgMur93Za4+whhBKy1X8p3P4WFnGbb9f/a79yynjtuNnE+AB+2FWbHgNvWaHtq1mz/eN0792oDV6RIlRXEWWtJxaLg1HDkGY9GrV/MJ08czrc/3/RzWj4hd6fSRnOm4egg/WttnSSlHmvPqGPpXMaPGA3AEjxU7FGpTaRcN1O+SGuJAxOoNmB+MJbFj+St7Ll+dNEbh/tH71uU+mNuObv8Abj/cqhT6LOMRWZmQJBkkbiCJmFivehV0ILC2p4AwR72PpXNfHgnxMt62yR6hdvzhtXiRVZ4gmD7D6FqXPSC1bvVBPcJK57V6P3gMdS+e/lvG4wtarwe6Eg03A9xBzMbYjmFavDY/i6tstvpebnpRbfJLnH1D6z9ivPkm4k2vTuC1sQ9gOZ+SfBcBZw64c7SKb5zyO4BMbb4K7Z5CeHVqNG6FVsF1RhAkHAaRyK6K4qV9T5ZTe0tDyrVIvh/yaf7dVUx9wAr/AOUvo7e17wVKFB1RnUsbqBaO0HVCR2nDk4e1VX8yuJ87N/vU/vrqi3h5mXHabz4V51yTvMcvFZqTHdx9ZU9+aHFAP6nU9AdT++tKr0R40drKo0d2ql/EUTZNcd59RpqG2pAanxPhk/SsFW/AGljY+vHeVuP6EcZg/wBDqn/vp/xFjpdCOMCZsavv0v4irN/HhrThvP8AJEvuXTPtHIrxQsnvB0uBA+QTBPg0qb/MnjH+Aqe9S/iLJb9CeMNIPwKpj51MeyHqk5PuG8YJ90mIlUbmjVEtAM7QRn2wtFuoDtNLSMT8kxAiV0Ol0O4yxr9NpUOrvdT37/P3UW7oFxmT/QKkEz59L76ztETG6w3pNotq0/pWPhRiBI7u/PerB0ArTxKyJM/HMA8MyvlXyccZB1Msqg8NdOP21O9DugnFaXEbStVtHNYyo0vfqZgd5AdJ/FU6ce3R1piJiVW8oFSOJ38/4h/2fuUTQrHA5HvGM+lXnpl0D4tV4hd1aVo91N9Z7mODqYDgYgwXgqHb5OuNAH+g1PfpffWsw5JhE2dWmXk1MMM6iAHZOxAkH6cSp6lYWVSnPXvYOcUpJ9ANSdoWCj5OOM4Bsqnv0vvq48J6A8Rp6IY9kNzDmzMyflc/WrVrG9qzGlMv+DWtINDrmrBkg9TzwMy9a1vbWYd/WKpEY+KGTnft7K/cd6A8RrPB0uIgTJYIO3LJwopvkwvmkRReTzywDY7HUVfmFTuqgzkbCDEYAG/qWB7+xAGYzHhG/wDPNWW98nXFvk2jz6HM+1wWn/8Az7jQECyq+PbpfxFW3k1tVCZdnx/krHUce9Wxvk74zObGp79P+IvB8m3GP8FV96l/ETayoly6J5Cf/kav/SVv26KhHeTPjX+Cqe9S++rv5IuhvErW9qVLi2dTYbaqwOLmGXudTIHZcf1T7FEpQFtVMA+CzuqU3wHsa4fOaD9aUOhnGNLQbKrgZ7dLJ2/XW3Q6IcTbvZVT/wB1Lb1vSaKxZo1eBWbzqdTDMZ0ksB2GRtyWtU6K2ziNL3NE5ktOIO0jvjeVLVuivGDA+B1IGw1U/vrNQ6I8Uluq0qDvh1Pv5dv1qenaI3tHPEzrSqX3RGozLHUniYHyXR39x9q93PRyrSpl4Ns9oYSS5wB8Q3V2cASHYOSrfQ6I8SaHf0Z8g9mSw89x2+4LNS6OcTAaTZvJbMZp4J5nt5Uat9p5o+lS4JYXPxb/AIJbYBh2sAv3YdRplwMR9M7yr3wOs9hOptAN6suc1oIAIJBh05IiRLc94yRH1ejHFHt7Vo6T85nedhqwTifQtm66McRdTP8ARnatIbALMAch2lhl4ecnuYaUzxX1EvNxxB+nUWFskjvjJ3bggYz3LRr8Se0SHNdkghuDj05/9rcodG+Inz7N+BAOqnt3GDn8F8qdE+IOH9WqA7Yczbu87ZTThoqWz7+EZV4yYy09xzJB8RhdB8lNz1lO4Pc9g7uR3VAqdCuJt821efXT++ug+S3hd1QZcfCaRplz2Fslp1QDJ7BK06Na+YUjLNvEwvKIilIiIgIiICIiAiIgIiICIiAozjHHKVuWh+ol3JomAOZypNal/wAMo1tPWMDtO0z9m48EJZzWbp1z2YmfCJWOzvG1AS2cbgrOWiIjG0eCx29uxghoiUGKrfsa8MMyY9AnZZbq4axpc7bwXx9qwuDy0ahsV7q0muBa4SCg821w17Q5u3isq8UqTWgBogBe0BERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERB//Z" alt="Card image cap">
						<div class="card-block">
							<h4 class="card-title">Slider</h4>
							<p class="card-text">Section to edit Slider</p>
							<a href="{{ url('/slide') }}" class="btn btn-primary">Go there!</a>
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="col-md-3 mt-1">
			<div class="">
				<div class="card">
					<div class="card-body">
						<img class="card-img-top img-fluid" style="max-height: 130px; width: 100%; object-fit: cover;" src="https://static1.squarespace.com/static/52eec360e4b0c81c80749630/t/53cd83dbe4b09390906375c9/1408402670660/newspapers.jpg" alt="Card image cap">
						<div class="card-block">
							<h4 class="card-title">Article</h4>
							<p class="card-text">Section to edit Article</p>
							<a href="{{ url('/article') }}" class="btn btn-primary">Go there!</a>
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="col-md-3 mt-1">
			<div class="">
				<div class="card">
					<div class="card-body">
						<img class="card-img-top img-fluid" style="max-height: 130px; width: 100%; object-fit: cover;" src="https://www.rwsentosa.com/-/media/project/non-gaming/rwsentosa/mice/seaaquarium-openoceangallery.jpg?mh=666&la=en&h=666&w=1364&mw=1366&hash=F53EA2E27F963546F2D8CAACE917BFEB7D848BB0" alt="Card image cap">
						<div class="card-block">
							<h4 class="card-title">Event</h4>
							<p class="card-text">Section to edit Event</p>
							<a href="{{ url('/event') }}" class="btn btn-primary">Go there!</a>
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="col-md-3 mt-1">
			<div class="">
				<div class="card">
					<div class="card-body">
						<img class="card-img-top img-fluid" style="max-height: 130px; width: 100%; object-fit: cover;" src="https://www.aquascapeinc.com/upload/53039_MediumPondlessWaterfallKitWithAquaSurge2000-4000_Front.jpg" alt="Card image cap">
						<div class="card-block">
							<h4 class="card-title">Product</h4>
							<p class="card-text">Section to edit Product</p>
							<a href="{{ url('/product') }}" class="btn btn-primary">Go there!</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Columns section end -->
    </div>
  </div>
</div>
@endsection