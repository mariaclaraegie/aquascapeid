@extends('layouts.cms')

@section('content')
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Events</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item active">Create Event</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Configuration option table -->
		<section id="configuration">
			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Edit Event {{$data->id}}</h4>
							<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
						</div>
						<div class="card-body collapse in">
            <form action="{{ route('event.update', $data->id) }}" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{ method_field('PUT') }}
              <div class="card-block card-dashboard">
                <label><strong>PHOTO</strong></label><br/>
                <img src="/image/event/{{$data->photo}}" style="max-height: 200px; object-fit: cover; width: 100%;" alt="Los Angeles">
                <br/><br/>
                <input name="photo" type="file" />
              </div>
              <div class="card-block card-dashboard">		
                <div class="col-md-12">
                  <!-- <fieldset class="form-group ">
                    <label><strong>Start Date</strong></label>
                    <input type='date' class="form-control" id="datetimepicker4"/>
                  </fieldset> -->
                  <fieldset class="form-group">
                      <label><strong>JUDUL</strong></label>
                      <input type="text" class="form-control" id="basicInput" name="title" value="{{$data->title == 'Event '.$data->id ? '' : $data->title}}">
                  </fieldset>
                  <fieldset class="form-group">
                    <label><strong>DESKRIPSI</strong></label>
                    <textarea id="" class="form-control" name="desc">{{$data->desc == 'Event '.$data->id ? '' : $data->desc}}</textarea>
                  </fieldset> 
                  <fieldset class="form-group">
                      <label><strong>TITLE</strong></label>
                      <input type="text" class="form-control" id="basicInput" name="judul" value="{{$data->judul}}">
                  </fieldset>
                  <fieldset class="form-group">
                    <label><strong>CONTENT</strong></label>
                    <textarea id="" class="form-control" name="deskripsi">{{$data->deskripsi}}</textarea>
                  </fieldset> 
                  <button type="submit" class="btn btn-primary">Submit</button>	
                </div>
							</div>
              </form>
						</div>
					</div>
				</div>
			</div>
		</section>
        </div>
      </div>
    </div>


<script src="{{asset('app-assets/ckeditor/ckeditor.js')}}"></script>

    <script>
    var konten = document.getElementById("konten")
      CKEDITOR.replace(konten,{
      language:'en-gb'
    })
    CKEDITOR.config.allowedContent = true
</script>
@endsection