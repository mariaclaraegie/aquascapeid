@extends('layouts.cms')

@section('content')
    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Slider</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item active">Photo Slider
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Configuration option table -->
		<section id="configuration">
			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Slider</h4>
							<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                        </div>
                            <section id="configuration">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="card">
                                       
                                            <div class="card-body collapse in">
                                                <div class="card-block card-dashboard">
                                                @foreach($datas as $data)
                                                    <div class="col-md-4">
                                                        <div class="carousel-item">
                                                        <label for="input-16"><strong>Slider {{$loop->index+1}}</strong></label>
                                                        <br/>
                                                            <img src="/image/slide/{{$data->photo}}" style="max-height: 200px; object-fit: cover; width: 100%;" alt="Los Angeles">
                                                            <fieldset>
                                                                <label for="input-16">Title : {{$data->title}}</label>
                                                            </fieldset> 
                                                            <fieldset>
                                                                <label for="input-16">Desc : {{$data->decs}}</label>
                                                            </fieldset> 
                                                            <form action="{{ route('slide.edit', $data->id) }}" method="post">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                                <a href="{{ route('slide.edit', $data->id) }}" class="btn btn-primary">Edit slide</a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
						</div>
					</div>
				</div>
			</div>
		</section>
        </div>
      </div>
    </div>
<style>
    .carousel-inner {
        width: 100%;
        text-align: center;
        height: 400px;
        background-size: cover;
    }
    .carousel-item {
        background-size: cover;
        width: 100%;
    }
    .carousel-item img {
        text-align: center;
    }
    #demo {
        text-align: center;
    }
    .wrapper-slider {
        text-align: center;
    }
</style>
@endsection