@extends('layouts.cms')

@section('content')
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Article</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item active">Create Article
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Configuration option table -->
		<section id="configuration">
			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Create New Post</h4>
							<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
						</div>
						<div class="card-body collapse in">
            <form action="{{ route('article.store') }}" method="POST" enctype="multipart/form-data">
              <input type="hidden" value="{{csrf_token()}}" name="_token" />
              <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
              <div class="card-block card-dashboard">
                <label>Photo</label>
                <input name="photo" type="file" />
              </div>
              <div class="card-block card-dashboard">			
                <div class="col-md-12">
                  <fieldset>
                    <input type="checkbox" id="input-16" name="status" value=1>
                    <label for="input-16">Pubished?</label>
                  </fieldset> 
                  <fieldset class="form-group">
                      <label><strong>TITLE IN BAHASA</strong></label>
                      <input type="text" class="form-control" id="basicInput" name="judul">
                  </fieldset>
                  <fieldset class="form-group">
                    <label><strong>CONTENT IN BAHASA</strong></label>
                    <textarea id="konten" class="form-control" name="isi"></textarea>
                  </fieldset> 
                  <fieldset class="form-group">
                      <label><strong>TITLE IN ENGLISH</strong></label>
                      <input type="text" class="form-control" id="basicInput" name="title">
                  </fieldset>
                  <fieldset class="form-group">
                    <label><strong>CONTENT IN ENGLISH</strong></label>
                    <textarea id="article" class="form-control" name="article"></textarea>
                  </fieldset> 
                  <button type="submit" class="btn btn-primary">Submit</button>	
                </div>
							</div>
              </form>
						</div>
					</div>
				</div>
			</div>
		</section>
        </div>
      </div>
    </div>

    <script src="{{asset('app-assets/ckeditor/ckeditor.js')}}"></script>
    
    <script src="{{asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/switch.js') }}" type="text/javascript"></script>
    <script>
    var konten = document.getElementById("konten")
      CKEDITOR.replace(konten,{
      language:'en-gb'
    })
    CKEDITOR.config.allowedContent = true
    var article = document.getElementById("article")
      CKEDITOR.replace(article,{
      language:'en-gb'
    })
    CKEDITOR.config.allowedContent = true
</script>
@endsection