@extends('layouts.cms')

@section('content')
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 50px;
  height: 24px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Article</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item active">List Article
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Configuration option table -->
		<section id="configuration">
			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">List Article</h4><br>
							<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
              <a href="{{ route('article.create') }}" class="btn btn-primary">Add Article</a>	
						</div>
						<div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <table class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                    <td colspan=2></td>
                                    <td>No</td>
                                    <td>Title</td>
                                    <td>Created By</td>
                                    <!-- <td>Approved</td> -->
                                    <td>Status</td>
                                    <td>Approve?</td>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    @foreach($datas as $data)
                                    <tr>
                                        <td><button type="button" class="btn btn-warning"><a style="font-weight: 600; color: white"href="{{ URL::to('article/' . $data->id . '/edit') }}">Edit</a></button></td>
                                        <td>
                                        <form action="{{ route('article.destroy', $data->id) }}" method="post">
                                            {{ csrf_field() }}{{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                        </td>
                                        <td>{{$loop->index+1}}</td>
                                        <td><a href="{{ URL::to('article/' . $data->id) }}">{{$data->judul}}</a></td>
                                        <td>{{$data->user->name}}</td>
                                        <!-- <td>{!!$data->isi!!}</td> -->
                                        <td>{{$data->status == 1 ? 'Published' : 'Not Published'}}</td>
                                        @if(Auth::user()->role==3)
                                        <form action="{{ route('blog.update',$data->id) }}" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="_method" value="PUT">
                                        <td>
                                            <!-- <label class="switch"> -->
                                                <input type="submit" value="{{$data->approved == 1 ? 'Approved' : 'Waiting'}}" name="" class="btn btn-{{$data->approved == 1 ? 'primary' : 'danger'}}" >
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <!-- <span class="slider round"></span> -->
                                            <!-- </label> -->
                                        </td>
                                        </form>
                                        @else
                                         <td>{{$data->approved == 1 ? 'Approved' : 'Waiting'}}</td>
                                        @endif
                                        </tr>
                                        
                                    @endforeach                                        
                                </tbody>
                            </table>
                        </div>
						</div>
					</div>
				</div>
			</div>
		</section>
        </div>
      </div>
    </div>
@endsection