@extends('layouts.cms')

@section('content')
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Article</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item active">Show Article
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Configuration option table -->
		<section id="configuration">
			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">{{$data->judul}}</h4>
							<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
						</div>
						<div class="card-body collapse in">
							
                        <div class="card-block card-dashboard">
                            <img src="/image/article/{{$data->photo}}" alt="" width="800" height="400">
                        </div>
                        <div class="card-block card-dashboard">
                            <label for="title">{!!$data->isi!!}</label>
                        </div>
						</div>
					</div>
				</div>
			</div>
		</section>
        </div>
      </div>
    </div>
    

@endsection