@extends('layouts.cms')

@section('content')
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Product</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item active">Edit Product
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Configuration option table -->
		<section id="configuration">
			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Edit Product</h4>
							<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
						</div>
						<div class="card-body collapse in">
                        <form action="{{ route('product.update',$data->id) }}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" value="{{csrf_token()}}" name="_token" />
                        <input type="hidden" name="_method" value="PUT">
                        <div class="card-block card-dashboard">			
                            <div class="col-md-12">
                            <fieldset class="form-group">
                                <label><strong>PRODUCT NAME</strong></label>
                                <input type="text" class="form-control" id="" name="title" value="{{$data->title}}">
                            </fieldset>
                            <fieldset class="form-group">
                                <label><strong>PRICE</strong></label>   
                                <div class="input-group">          
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" id="netprice" value="{{$data->price}}" class="form-control no-spinners" placeholder="" name="price" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Net Price">            
                                </div>
                            </fieldset>
                            <!-- <fieldset class="form-group">
                                <label><strong>TYPE</strong></label>
                                <input type="text" class="form-control" id="" name="type" value="{{$data->type}}">
                            </fieldset>
                            <fieldset class="form-group">
                                <label><strong>ORIGIN</strong></label>
                                <input value="{{$data->origin}}" type="text" class="form-control" id="" name="origin" >
                            </fieldset>
                            <fieldset class="form-group">
                                <label><strong>GROWTH RATE</strong></label>
                                <input type="text" class="form-control" id="" name="growth" value="{{$data->growth}}">
                            </fieldset> -->
                            <fieldset class="form-group">
                                <label><strong>SIZE</strong></label>
                                <input type="text" class="form-control" id="" name="height" value="{{$data->height}}">
                            </fieldset>
                            <!-- <fieldset class="form-group">
                                <label><strong>LIGHT DEMAND</strong></label>
                                <input type="text" class="form-control" id="" name="lightdemand" value="{{$data->lightdemand}}">
                            </fieldset>
                            <fieldset class="form-group">
                                <label><strong>CO<sup>2</sup></strong></label>
                                <input type="text" class="form-control" id="" name="co" value="{{$data->co}}">
                            </fieldset> -->
                            <fieldset class="form-group">
                            
                                <label><strong>DESKRIPSI</strong></label>
                                <textarea required id="decs" rows="5" class="form-control" name="decs" placeholder="" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="address">{{$data->decs}}</textarea>
                            </fieldset>
                            <fieldset class="form-group">
                                <label><strong>DESCRIPTION</strong></label>
                                <textarea id="decs" rows="5" class="form-control" name="leveldesc" placeholder="" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="address">{{$data->leveldesc}}</textarea>
                            </fieldset>
                            <!-- <fieldset class="form-group">
                                <label><strong>LEVEL</strong></label>
                                <select id="level" name="level" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
                                    <option value="1" {{$data->level == '1' ? 'selected' :''}}>Easy</option>
                                    <option value="2" {{$data->level == '2' ? 'selected' :''}}>Medium</option>
                                    <option value="3" {{$data->level == '3' ? 'selected' :''}}>Advance</option>
                                </select>
                            </fieldset> -->
                            <fieldset class="form-group">
                                <label><strong>CATEGORY</strong></label>
                                <select id="subcategory_id" name="subcategory_id" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
                                    @foreach($categorys as $category)
                                        <option value="{{$category->id}}" {{$category->id == $data->subcategory->id ? 'selected' :''}}>{{$category->category}}</option>
                                    @endforeach
                                </select>
                            </fieldset> 
                             <!-- <fieldset class="form-group">
                                <label><strong>SUB CATEGORY</strong></label>
                                <select id="subcategory" name="subcategory_id" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
                                    <option value="{{$data->subcategory->id}}">{{$data->subcategory->category}}</option>
                                </select>
                            </fieldset>  -->

                            <button type="submit" class="btn btn-primary">Submit</button>	
                            </div>
                            </div>
                        </from>
						</div>
					</div>
				</div>
			</div>
		</section>
        </div>
      </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
//      $(document).ready(function(){
//         $('#category').on('change', function(e){
//            var category_id = e.target.value;
//            $.get('{{ url('getsubcategory') }}/' + category_id, function(data) {
//            //console.log(data);
//            $('#subcategory').empty();
//            $.each(data,function(key, value) {
//             $('#subcategory').append('<option value=' + value.id + '>' + value.category + '</option>');
//             });
//             });
//         });
//       });
    </script>
    <script src="{{asset('app-assets/ckeditor/ckeditor.js')}}"></script>
    
    <script src="{{asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/switch.js') }}" type="text/javascript"></script>
@endsection