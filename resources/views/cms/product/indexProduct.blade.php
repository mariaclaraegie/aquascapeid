@extends('layouts.cms')

@section('content')
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Product</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item active">List Product
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Configuration option table -->
		<section id="configuration">
			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">List Product</h4>
							<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
              <br/><br/>
              <a href="{{ route('product.create') }}" class="btn btn-primary">Add New Product</a>	
						</div>
						<div class="card-body collapse in">
            <table id="example" class="table table-striped table-bordered table-responsive" style="width:100%">
            <thead>
                <tr>
                    <th colspan="4"><center>Action</center></th>
                    <th>Item No.</th>
                    <th>Product Name</th>
                    <th>Desc</th>
                    <th>Show</th>
                </tr>
            </thead>
            <tbody>
            @foreach($datas as $data)
                <tr>
                    <td><a href="{{ route('product.edit',$data->id) }}" class="btn btn-success btn-md">Edit</a>	</td>
                  
                    <form action="{{ route('product.update',$data->id) }}" method="POST" enctype="multipart/form-data">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="_method" value="PUT">
                      <td>
                      <input type="submit" value="{{$data->is_hidden == 0 ? 'Hide' : 'Unhide'}}" name="" class="btn btn-light"  >
                      <input type="hidden" name="id" value="{{$data->id}}">
                      <input type="hidden" name="is_hidden" value="{{$data->is_hidden == 0 ? '1' : '0'}}">
                      </td>
                    </form>
                    <form action="{{ route('product.destroy', $data->id) }}" method="post">
                    {{ csrf_field() }}{{ method_field('DELETE') }}
                    <td><button type="submit" class="btn btn-danger btn-md">Delete</button></a>	</td>
                    </form>
                    <td><a href="{{ route('photo.edit',$data->id) }}" class="btn btn-primary btn-md">Gallery</a>	</td>
                    <td>{{$data->item_no}}</td>
                    <td><a href="{{ route('shop.show',$data->id) }} " target="_blank">{{$data->title}}</a></td>
                    <td>{{str_limit($data->decs,$limit=20)}}</td>
                    <td>{{$data->complete == 1 ? 'Yes' : 'No'}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
	          </div>
					</div>
				</div>
			</div>
		</section>
        </div>
      </div>
    </div>

    <script src="{{asset('app-assets/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/switch.js') }}" type="text/javascript"></script>

    <!-- javascript and css for table  -->
    
@endsection