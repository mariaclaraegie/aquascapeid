@extends('layouts.cms')

@section('content')
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Photo</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item active">Upload Photo
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Configuration option table -->
		<section id="configuration">
			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">{{$data->title}}</h4>
							<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                        </div>
						<div class="card-body collapse in">
                            
                            <div class="card-block card-dashboard">
                            <div class="col-md-12">
                            @foreach($data->gallerys as $gallery)
                                <fieldset class="form-group">
                                    <img src="/image/gallery/{{$gallery->photo}}" style="max-height: 200px; object-fit: cover; width: 100%;" alt="Los Angeles">
                                    <form action="{{ route('photo.destroy', $gallery->id) }}" method="post">
                                        {{ csrf_field() }}{{ method_field('DELETE') }}
                                        <td><button type="submit" class="btn btn-danger">Delete</button></a></td>
                                    </form>
                                </fieldset>
                            @endforeach
                            <form action="{{ route('photo.store') }}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                            <br/>
                            <input type="hidden" name="product_id" value="{{$data->id}}">
                                <label>Photo</label>
                                <input name="photo" type="file" />
                            </div>
                            </div>
                            <div class="card-block card-dashboard">	
                            <button type="submit" class="btn btn-primary">Submit</button>	
                            </div>
                            </form>
                        </div>                        
					</div>
				</div>
			</div>
		</section>
        </div>
      </div>
    </div>
    
    <script src="{{asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/switch.js') }}" type="text/javascript"></script>
@endsection