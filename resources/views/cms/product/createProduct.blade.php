@extends('layouts.cms')

@section('content')
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">Product</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item active">Create Product
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Configuration option table -->
		<section id="configuration">
			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Create New Product</h4>
							<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
						</div>
						<div class="card-body collapse in">
                        <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" value="{{csrf_token()}}" name="_token" />
                        <div class="card-block card-dashboard">			
                            <div class="col-md-12">
                            <fieldset class="form-group">
                                <label>Photo</label>
                                <input name="photo" type="file" />
                            </fieldset>
                            <fieldset class="form-group">
                                <label><strong>PRODUCT NAME</strong></label>
                                <input type="text" placeholder="Nama Produk" required class="form-control" id="basicInput" name="title">
                            </fieldset>
                            <fieldset class="form-group">
                                <label><strong>PRICE</strong></label>   
                                <div class="input-group">          
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" id="netprice" required class="form-control no-spinners" placeholder="wajib diisi, jika tidak tahu, masukkan angka 0" name="price" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Net Price">            
                                </div>
                            </fieldset>
                            <!-- <fieldset class="form-group">
                                <label><strong>TYPE</strong></label>
                                <input type="text" class="form-control" id="basicInput" name="type">
                            </fieldset>
                            <fieldset class="form-group">
                                <label><strong>ORIGIN</strong></label>
                                <input type="text" class="form-control" id="basicInput" name="origin">
                            </fieldset>
                            <fieldset class="form-group">
                                <label><strong>GROWTH</strong></label>
                                <input type="text" class="form-control" id="basicInput" name="growth">
                            </fieldset> -->
                            <fieldset class="form-group">
                                <label><strong>SIZE</strong></label>
                                <input type="text" class="form-control" placeholder="contoh: 5cm, 5 inch, 5 meter x 10 meter" id="basicInput" name="height">
                            </fieldset>
                            <!-- <fieldset class="form-group">
                                <label><strong>LIGHT DEMAND</strong></label>
                                <input type="text" class="form-control" id="basicInput" name="lightdemand">
                            </fieldset>
                            <fieldset class="form-group">
                                <label><strong>CO<sup>2</sup></strong></label>
                                <input type="text" class="form-control" id="basicInput" name="co">
                            </fieldset> -->
                            <fieldset class="form-group">
                                <label><strong>DESKRIPSI (ID)</strong></label>
                                <textarea required id="decs" rows="5" class="form-control" name="decs" placeholder="" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="address"></textarea>
                            </fieldset>
                            <fieldset class="form-group">
                                <label><strong>DESCRIPTION (ENG)</strong></label>
                                <textarea id="decs" rows="5" class="form-control" name="leveldesc" placeholder="" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="address"></textarea>
                            </fieldset>
                            <!-- <fieldset class="form-group">
                                <label><strong>LEVEL</strong></label>
                                <select id="level" name="level" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
                                    <option value="1">Easy</option>
                                    <option value="2">Medium</option>
                                    <option value="3">Advance</option>
                                </select>
                            </fieldset> -->
                           <input type ="hidden" name="level" value = "1">
                            <fieldset class="form-group">
                                <label><strong>CATEGORY</strong></label>
                                <select id="subcategory_id" name="subcategory_id" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
                                    <!-- <option value="">Choose Category</option> -->
                                    @foreach($categorys as $data)
                                        <option value="{{$data->id}}">{{$data->category}}</option>
                                    @endforeach
                                </select>
                            </fieldset>
                            <!-- <fieldset class="form-group">
                                <label><strong>SUB CATEGORY</strong></label>
                                <select id="subcategory" name="subcategory_id" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
                                    <option value=""></option>
                                </select>
                            </fieldset> -->
                            <button type="submit" class="btn btn-primary">Submit</button>	
                            </div>
                            </div>
                        </form>
						</div>
					</div>
				</div>
			</div>
		</section>
        </div>
      </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
//      $(document).ready(function(){
//         $('#category').on('change', function(e){
//            var category_id = e.target.value;
//            $.get('{{ url('getsubcategory') }}/' + category_id, function(data) {
//            //console.log(data);
//            $('#subcategory').empty();
//            $.each(data,function(key, value) {
//             $('#subcategory').append('<option value=' + value.id + '>' + value.category + '</option>');
//             });
//             });
//         });
//       });
    </script>
    <script src="{{asset('app-assets/ckeditor/ckeditor.js')}}"></script>
    
    <script src="{{asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/switch.js') }}" type="text/javascript"></script>
@endsection