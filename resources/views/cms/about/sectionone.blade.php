@extends('layouts.cms')

@section('content')
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">About Us</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item active">Section One
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Configuration option table -->
		<section id="configuration">
			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Edit Section One</h4>
						</div>
						<div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <form action="{{ route('article.store') }}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <div class="card-block card-dashboard">
                                    <label>Background Photo</label>
                                    <input name="photo" type="file" />
                                </div>
                                <div class="card-block card-dashboard">			
                                    <div class="col-md-12">
                                    <fieldset class="form-group">
                                        <label><strong>Youtube Company Profile Link Video</strong></label>
                                        <input type="text" class="form-control" id="basicInput" name="judul">
                                    </fieldset>
                                    <button type="submit" class="btn btn-primary">Save</button>	
                                    </div>
                                </div>
                            </form>
                        </div>
						</div>
					</div>
				</div>
			</div>
		</section>
        </div>
      </div>
    </div>
@endsection