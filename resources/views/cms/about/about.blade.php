@extends('layouts.cms')

@section('content')
<div class="app-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-2">
        <h3 class="content-header-title mb-0">About Us</h3>
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">About Us Section
                </li>
            </ol>
            </div>
        </div>
        </div>
    </div>
<!-- Columns section start -->
<section id="columns">
	<div class="row">
		<div class="col-xs-12 mt-3 mb-1">
			<p>You can edit about us here. About us divide per section. You can edit each section by clicking button below the picture</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 mt-1">
			<div class="">
				<div class="card">
					<div class="card-body">
						<img class="card-img-top img-fluid" src="../../../app-assets/images/sectionone.png" alt="Card image cap">
						<div class="card-block">
							<h4 class="card-title">Section One</h4>
							<p class="card-text">Section to edit background image and youtube video</p>
                            <br/>
							<a href="#" class="btn btn-primary">Go there!</a>
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="col-md-4 mt-1">
			<div class="">
				<div class="card">
					<div class="card-body">
						<img class="card-img-top img-fluid" src="../../../app-assets/images/sectiontwo.png" alt="Card image cap">
						<div class="card-block">
                        <br/>
                        <br/>
                        <br/>
							<h4 class="card-title">Section Two</h4>
							<p class="card-text">Section to edit description</p>
                            <br/>
							<a href="#" class="btn btn-primary">Go there!</a>
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="col-md-4 mt-1">
			<div class="">
				<div class="card">
					<div class="card-body">
						<img class="card-img-top img-fluid" src="../../../app-assets/images/sectionthree.png" alt="Card image cap">
						<div class="card-block">
							<h4 class="card-title">Section Three</h4>
							<p class="card-text">Section to edit another description and background image</p>
							<a href="#" class="btn btn-primary">Go there!</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Columns section end -->
        </div>
    </div>
</div>
@endsection