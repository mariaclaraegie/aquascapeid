@extends('layouts.cms')

@section('content')
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-2">
            <h3 class="content-header-title mb-0">About Us</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item active">Section Two
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Configuration option table -->
		<section id="configuration">
			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Edit Section Two</h4>
						</div>
						<div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <form action="{{ route('article.store') }}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <div class="card-block card-dashboard">			
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <h4 class="card-title">First Column</h4>
                                            <fieldset class="form-group">
                                                <label><strong>Title</strong></label>
                                                <input type="text" class="form-control" id="basicInput" name="judul">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label><strong>Sub-Title</strong></label>
                                                <input type="text" class="form-control" id="basicInput" name="judul">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label><strong>Content</strong></label>
                                                <textarea class="form-control" rows="9"></textarea>
                                            </fieldset>
                                            <br/>
                                        </div>
                                        <div class="col-md-4">
                                            <h4 class="card-title">Second Column</h4>
                                            <fieldset class="form-group">
                                                <label><strong>Title</strong></label>
                                                <input type="text" class="form-control" id="basicInput" name="judul">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label><strong>Sub-Title</strong></label>
                                                <input type="text" class="form-control" id="basicInput" name="judul">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label><strong>Content</strong></label>
                                                <textarea class="form-control" rows="9"></textarea>
                                            </fieldset>
                                            <br/>
                                        </div>
                                        <div class="col-md-4">
                                            <h4 class="card-title">Third Column</h4>
                                            <fieldset class="form-group">
                                                <label><strong>Title</strong></label>
                                                <input type="text" class="form-control" id="basicInput" name="judul">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label><strong>Sub-Title</strong></label>
                                                <input type="text" class="form-control" id="basicInput" name="judul">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label><strong>Content</strong></label>
                                                <textarea class="form-control" rows="9"></textarea>
                                            </fieldset>
                                            <br/>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-block btn-primary">Save</button>
                                        </div>
                                        <div class="col-md-4"></div>	
                                    </div>
                                </div>
                            </form>
                        </div>
						</div>
					</div>
				</div>
			</div>
		</section>
        </div>
      </div>
    </div>
@endsection