<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">

    <title>About Us</title>

    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="/assets/favicon.png" type="image/x-icon"/>

    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700%7CPlayfair+Display:400,400i"
          rel="stylesheet">

    <!--=== All Plugins CSS ===-->
    <link href="{{ asset('assets/front-assets/css/plugins.css') }}" rel="stylesheet">
    <!--=== All Vendor CSS ===-->
    <link href="{{ asset('assets/front-assets/css/vendor.css') }}" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="{{ asset('assets/front-assets/css/style.css') }}" rel="stylesheet">
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">

    <!-- Modernizer JS -->
    <script src="{{ asset('assets/front-assets/js/modernizr-2.8.3.min.js') }}"></script>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="preloader-active">
<style>
    html, body {
        overflow-x: hidden;
    }
</style>
<!--== Start PreLoader Wrap ==-->
<div class="preloader-area-wrap">
    <div class="spinner d-flex justify-content-center align-items-center h-100">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<!--== End PreLoader Wrap ==-->

<!--== Start Header Area Wrapper ==-->
<header class="header-area-wrapper black-header header-padding sticky-header" style="background-color: black;">
    <div class="container-fluid">
        <div class="row">
            <!-- Mobile Responsive Menu -->
            <div class="col-3 d-block d-lg-none">
                <button class="mobile-menu"><i class="fa fa-bars"></i></button>
            </div>

            <!-- Start Logo Area Wrap -->
            <div class="col-6 col-lg-2 col-xl-3 text-center text-lg-left">
                <a href="{{ url('/home') }}" class="logo-wrap d-block">
                    <img src="/assets/front-assets/img/brand-logo/logo.png" style="width:270px;" class="logos sticky-logo" alt="Black Logo"/>
                </a>
            </div>
            <!-- End Logo Area Wrap -->

            <!-- Start Main Navigation Wrap -->
            <div class="col-3 col-lg-10 col-xl-9 my-auto ml-auto position-static">
                <div class="header-right-area d-flex justify-content-end align-items-center">
                    <div class="navigation-area-wrap d-none d-lg-block">
                        <nav class="main-navigation" style="padding-right: 60px;">
                            <ul class="main-menu nav justify-content-end">
                                <li class="dropdown-navbar"><a href="{{ url('/home') }}">@lang('home.home')</a></li>
                                <li class="dropdown-navbar arrow"><a href="{{ url('/shop') }}">@lang('home.product')</a>
                                    <ul class="dropdown-nav">
                                    @foreach($categorys as $category)
                                        <li class="dropdown-navbar arrow"><a href="{{ url('/category/'.$category->id) }}">{{$category->category}}</a>
                                        </li>
                                    @endforeach
                                    </ul>
                                </li>
                                <li class="dropdown-navbar"><a href="{{ url('/front/about') }}">@lang('home.aboutus')</a></li>
                                <li class="dropdown-navbar"><a href="/blog">@lang('home.article')</a></li>
                                <li class="dropdown-navbar"><a href="/events">@lang('home.event')</a></li>
                                <li class="dropdown-navbar"><a href="{{ url('/front/contactus') }}">@lang('home.contactus')</a></li>
                                <li><a href="locale/id">ID </a> </li> <li class="eng"><a href="locale/en">ENG</a></li>
                                <!-- @guest
                                    <li class="dropdown-navbar"><a href="{{ route('login') }}">Login</a></li>
                                @else
                                    <li class="dropdown-navbar"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout </a>
                                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></li>
                                @endif -->
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- End Main Navigation Wrap -->
        </div>
    </div>
</header>
<!--== End Header Area Wrapper ==-->

@yield('content')

<!--== Start Footer Area Wrapper ==-->
<footer class="footer-wrapper layout--4">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="footer-contact-information mt-100 mt-sm-80 mt-xs-30">
                    <div class="row mtm-30">
                        <div class="col-sm-6 text-center">
                            <div class="single-footer-con-info">
                                <h2>Contact.</h2>
                                <p> +62 21 786 7590</p>
                                <a href="mailto:info@aquascape-indonesia.com">info@aquascape-indonesia.com</a>
                                <a href="http://aquascape-indonesia.com">www.aquascape-indonesia.com</a>
                            </div>
                        </div>

                        <div class="col-sm-6 text-center">
                            <div class="single-footer-con-info">
                                <h2>Office.</h2>
                                <address>
                                    <p>PT.  BUMI AQUA NUSANTARA <br>
                                    Cilandak Warehouse Commercial Area 410, Jalan Raya Cilandak KKO <br>
                                    Cilandak Timur, Pasar Minggu, Jakarta Selatan 12560 <br></p>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--== End Footer Area Wrapper ==-->

<!--== Start Off Canvas Area Wrapper ==-->
<aside class="off-canvas-area-wrapper">
    <!-- Off Canvas Overlay -->
    <div class="off-canvas-overlay"></div>

    <!-- Start Off Canvas Content Area -->
    <div class="off-canvas-content-wrap">
        <div class="off-canvas-content">
            <!-- Start Search Box Wrap -->
            <div class="search-box-wrap off-canvas-item">
                <form action="#" method="post">
                    <input type="search" placeholder="Search.."/>
                    <button class="btn-search"><i class="fa fa-search"></i></button>
                </form>
            </div>

            <!-- Start About Content Wrap -->
            <div class="about-content off-canvas-item">
                <h2>Who We Are</h2>
                <p>Organic seitan post-ironic, four loko bicycle rights art party tousled. Mlkshk tote bag
                    stumptown.</p>
            </div>

            <!-- Start Useful Links Content -->
            <div class="useful-link-wrap off-canvas-item">
                <h2>Useful Links</h2>

                <ul class="useful-link-menu">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="shop.html">Shop</a></li>
                    <li><a href="about.html">About Us</a></li>
                    <li><a href="blog.html">Blog</a></li>
                    <li><a href="contact.html">Contact</a></li>
                </ul>
            </div>

            <!-- Start Social Links Content -->
            <div class="social-links-wrap off-canvas-item">
                <h2>Connect</h2>

                <div class="social-links">
                    <a href="#" class="trio-tooltip" data-tippy-content="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="trio-tooltip" data-tippy-content="Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="trio-tooltip" data-tippy-content="Pinterest"><i class="fa fa-pinterest"></i></a>
                    <a href="#" class="trio-tooltip" data-tippy-content="Instagram"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>

        <!-- Off Canvas Close Icon -->
        <button class="btn-close trio-tooltip" data-tippy-content="Close" data-tippy-placement="left"><i
                class="fa fa-close"></i></button>
    </div>
    <!-- End Off Canvas Content Area -->
</aside>
<!--== End Off Canvas Area Wrapper ==-->

<!--== Start Off Canvas Area Wrapper ==-->
<aside class="off-canvas-responsive-menu">
    <!-- Off Canvas Overlay -->
    <div class="off-canvas-overlay"></div>

    <!-- Start Off Canvas Content Area -->
    <div class="off-canvas-content-wrap">
        <div class="off-canvas-content">

        </div>
        <!-- Off Canvas Close Icon -->
        <button class="btn-close trio-tooltip" data-tippy-content="Close" data-tippy-placement="right"><i
                class="fa fa-close"></i></button>
    </div>
    <!-- End Off Canvas Content Area -->
</aside>
<!--== End Off Canvas Area Wrapper ==-->

<!--== Start Off Canvas Area Wrapper ==-->
<aside class="off-canvas-search-box">
    <!-- Off Canvas Overlay -->
    <div class="off-canvas-overlay"></div>

    <!--== Start Search Box Area ==-->
    <div class="search-box-wrapper text-center">
        <div class="search-box-content">
            <form action="#" method="post">
                <input type="search" placeholder="Search"/>
                <button class="btn-search"><i class="fa fa-search"></i></button>
            </form>
        </div>
    </div>
    <!--== End Search Box Area ==-->
</aside>
<!--== End Off Canvas Area Wrapper ==-->

<!--=======================Javascript============================-->
<!--=== All Vendor Js ===-->
<script src="{{ asset('assets/front-assets/js/vendor.js') }}"></script>
<!--=== All Plugins Js ===-->
<script src="{{ asset('assets/front-assets/js/plugins.js') }}"></script>
<!--=== Active Js ===-->
<script src="{{ asset('assets/front-assets/js/active.min.js') }}"></script>

<!--=== Revolution Slider Js ===-->
<script src="{{ asset('assets/front-assets/js/revslider/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/jquery.themepunch.revolution.min.js') }}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.video.min.js') }}"></script>

<script src="{{ asset('assets/front-assets/js/revslider/revslider-active.js') }}"></script>
</body>
</html>