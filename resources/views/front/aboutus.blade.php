@extends('layouts.front')

@section('content')

<!--== Start Page Content Wrapper ==-->
<div class="page-wrapper">
    <!--== Start About Hero Area Wrapper ==-->
    <section class="about-hero-area hv-100 parallaxBg bg-img" data-bg="https://7fc89099b105d242abe88e47-thegreenmachinel.netdna-ssl.com/wp-content/uploads/2018/07/TGM-Book-SPO_0131.jpg">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-lg-7 text-center m-auto">
                    <div class="about-us-content white mt-sm-70 mt-md-70">
                        <h2>About Us</h2>
                        <p>We’re a creative agency who pursue brilliance and use culture to create progress in the
                            world.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--== End About Hero Area Wrapper ==-->

    <!--== Start About Description Content Area ==-->
    <section class="about-desc-content-area pb-120 pb-md-80 pb-sm-60">
        <div class="container">
            <!-- Start Video Area  -->
            <div class="video-popup-area">
                <div class="row">
                    <div class="col-lg-10 m-auto">
                        <figure class="video-popup-content">
                            <img src="https://s1.bukalapak.com/img/1925916683/w-1000/Aquascape.jpg"  class="bg-cover-300" style="height: 550px;" alt="About"/>
                            <figcaption class="video-popup-btn">
                                <a href="https://www.youtube.com/watch?v=7e90gBu4pas" class="btn-video-popup"><i
                                        class="fa fa-play"></i></a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
            <!-- End Video Area  -->

            <div class="row mt-44 mt-sm-18 mt-md-16">
                <!-- Start About Content -->
                <div class="col-sm-6 col-lg-4">
                    <div class="about-desc-item">
                        <h3>About.</h3>
                        <h5>Coolest Team On Earth</h5>
                        <p class="m-0">Tri-O is a full-service, award-winning advertising agency. We deliver innovative
                            solutions across media, marketing, content, insights, and campaign management.</p>
                    </div>
                </div>
                <!-- End About Content -->

                <!-- Start Mission Content -->
                <div class="col-sm-6 col-lg-4">
                    <div class="about-desc-item">
                        <h3>Mission.</h3>
                        <h5>Coolest Team On Mission</h5>
                        <p class="m-0">Since the day we started we have always strove for producing sales-driven
                            campaigns for premium must-see productions, venues, exhibitions and events.</p>
                    </div>
                </div>
                <!-- End Mission Content -->

                <!-- Start Awards Content -->
                <div class="col-sm-6 col-lg-4">
                    <div class="about-desc-item">
                        <h3>Awards.</h3>
                        <h5>Success Is Liking Yourself</h5>
                        <p class="m-0">We pride ourselves on creating campaigns that communicate a powerful unifying
                            message that is flexible enough to resonate across widely varied channels.</p>
                    </div>
                </div>
                <!-- End Awards Content -->
            </div>
        </div>
    </section>
    <!--== End About Description Content Area ==-->

    <!--== Start About Content Area ==-->
    <section class="about-area-content bg-img pt-120 pt-md-80 pt-sm-60 pb-120 pb-md-80 pb-sm-60"
             data-bg="http://www.aquarium-site.com/wp-content/uploads/2018/12/captivating-aquascape-design-of-aquascaping-love-planted-aquarium-substrate-slopes-31.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="about-area-content-left">
                        <div class="fashion-call-action-left">
                            <h2>Something <span>amazing</span> has happened!</h2>
                            <p>After 6 years of designing for good causes and good people, we are taking things to the
                                next level.</p>
                        </div>

                        <div class="event-about-content-wrap mt-34">
                            <div class="event-about-accordion" id="eventAboutAccordion">
                                <!-- Start Event Accordion #01 -->
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h3 data-toggle="collapse" data-target="#collapseOne" aria-expanded="true">
                                            General Information
                                            <span class="icons">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                        </h3>
                                    </div>

                                    <div id="collapseOne" class="collapse show" data-parent="#eventAboutAccordion">
                                        <div class="card-body">
                                            <p>Keytar DIY cred thundercats direct trade viral umami, fanny pack ugh
                                                authen
                                                shabby chic chartreuse. Lo-fi roof party.Lorem ipsum dolor sit amet, ut
                                                vidis
                                                commune scriptorem. Ad his suavitate com plectitur ruis dicant</p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Start Event Accordion #02 -->
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h3 data-toggle="collapse" data-target="#collapseTwo">
                                            Workshops
                                            <span class="icons">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                        </h3>
                                    </div>
                                    <div id="collapseTwo" class="collapse" data-parent="#eventAboutAccordion">
                                        <div class="card-body">
                                            <p>Keytar DIY cred thundercats direct trade viral umami, fanny pack ugh
                                                authen
                                                shabby chic chartreuse. Lo-fi roof party.Lorem ipsum dolor sit amet, ut
                                                vidis
                                                commune scriptorem. Ad his suavitate</p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Start Event Accordion #03 -->
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h3 data-toggle="collapse" data-target="#collapseThree">
                                            Sponsor Information
                                            <span class="icons">
                                        <i class="fa fa-plus"></i>
                                        <i class="fa fa-minus"></i>
                                    </span>
                                        </h3>
                                    </div>
                                    <div id="collapseThree" class="collapse" data-parent="#eventAboutAccordion">
                                        <div class="card-body">
                                            <p>Keytar DIY cred thundercats direct trade viral umami, fanny pack ugh
                                                authen
                                                shabby chic chartreuse. Lo-fi roof party.Lorem ipsum dolor sit amet, ut
                                                vidis
                                                commune scriptorem.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--== End About Content Area ==-->

</div>
<!--== End Page Content Wrapper ==-->
@endsection