@extends('layouts.front')
@section('content')
<!--== Start Page Header Area ==-->
<div class="page-header-wrapper bg-offwhite">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="page-header-content d-flex">
                    <h1>{{$data->title}}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!--== End Page Header Area ==-->

<!--== Start Page Content Wrapper ==-->
<div class="page-wrapper">
    <div class="product-details-content-wrap mt-120 mt-md-80 mt-sm-60">
        <div class="container">
            <div class="row">
                <!-- Start Product Thumbnail Area -->
                <div class="col-lg-6">
                    <div class="product-thumb-area element-sticky mb-sm-24 mb-md-30">
                        <div class="ht-slick-slider prod-thumb-inner"
                             data-slick='{"slidesToShow": 1, "infinite": false, "responsive": [{"breakpoint": 991,"settings": {"dots": false}}]}'>
                            @foreach($data->gallerys as $gallery)
                            <figure class="port-details-thumb-item">
                                <a href="/image/gallery/{{$gallery->photo}}" class="btn-img-gallery">
                                    <img class="bg-cover-500" src="/image/gallery/{{$gallery->photo}}" alt="Portfolio"/>
                                </a>
                            </figure>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- End Product Thumbnail Area -->

                <!-- Start Product Info Area -->
                <div class="col-lg-6">
                    <div class="product-details-info-content-wrap">
                        <div class="prod-details-info-content">
                            <h2>{{$data->title}}</h2>
                            <p></p>
                            <div class="row">
                                <!-- <div class="col-sm-6">
                                    <div class="port-details-con-inner">
                                        <div class="single-post-details__footer m-0">
                                            <div class="single-post-details__footer__item">
                                                <div class="footer-item-left">
                                                    <h5 class="item-head"><i class="fa fa-tags"></i> Level</h5>
                                                </div>
                                                <div class="footer-item-right">
                                                    <ul class="cate-list nav">
                                                        <li><a href="#">{{$data->level = 1 ? 'Easy' : $data->level = 2 ? 'Medium' : 'Advanced'}}</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-sm-6">
                                    <div class="port-details-con-inner">
                                        <div class="single-post-details__footer m-0">
                                            <div class="single-post-details__footer__item">
                                                <div class="footer-item-left">
                                                    <h5 class="item-head"><i class="fa fa-tags"></i> Category</h5>
                                                </div>
                                                <div class="footer-item-right">
                                                    <ul class="cate-list nav">
                                                        <li><a href="#">{{$data->subcategory->category}}</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="product-description-review mt-40">
                            <!-- Product Description Tab Menu -->
                            <ul class="nav nav-tabs desc-review-tab-menu" id="desc-review-tab" role="tablist">
                                <li>
                                    <a class="active" id="desc-tab" data-toggle="tab" href="#descriptionContent"
                                       role="tab">Description</a>
                                </li>
                                
                                <!-- <li>
                                    <a id="profile-tab" data-toggle="tab" href="#reviewContent">Info</a>
                                </li> -->
                               
                            </ul>

                            <!-- Product Description Tab Content -->
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="descriptionContent">
                                    <div class="description-content">
                                        <p>{{$locale == 'id' ? $data->decs : $data->leveldesc}}</p>
                                    </div>
                                    <div class="description-content">
                                        <p>@if($data->height != null || $data->height!= "")
                                            Ukuran : {{$data->height}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                               
                                <!-- <div class="tab-pane fade" id="reviewContent">
                                    <div class="product-ratting-wrap">
                                    <div class="about-area-content-left">
                                        <div class="fashion-call-action-left">
                                            <div class="event-about-content-wrap">
                                                <div class="event-about-accordion" id="eventAboutAccordion">
                                                    
                                                    <div class="card">
                                                        <div class="card-header" id="headingOne">
                                                            <h3 data-toggle="collapse" data-target="#collapseOne" aria-expanded="true">
                                                                Type
                                                                <span class="icons">
                                                            <i class="fa fa-plus"></i>
                                                            <i class="fa fa-minus"></i>
                                                        </span>
                                                            </h3>
                                                        </div>

                                                        <div id="collapseOne" class="collapse show" data-parent="#eventAboutAccordion">
                                                            <div class="card-body">
                                                                <p>{{$data->type}}</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                   
                                                    <div class="card">
                                                        <div class="card-header" id="headingTwo">
                                                            <h3 data-toggle="collapse" data-target="#collapseTwo">
                                                                Origin
                                                                <span class="icons">
                                                            <i class="fa fa-plus"></i>
                                                            <i class="fa fa-minus"></i>
                                                        </span>
                                                            </h3>
                                                        </div>
                                                        <div id="collapseTwo" class="collapse" data-parent="#eventAboutAccordion">
                                                            <div class="card-body">
                                                                <p>{{$data->origin}}</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                   
                                                    <div class="card">
                                                        <div class="card-header" id="headingThree">
                                                            <h3 data-toggle="collapse" data-target="#collapseThree">
                                                                Growth
                                                                <span class="icons">
                                                            <i class="fa fa-plus"></i>
                                                            <i class="fa fa-minus"></i>
                                                        </span>
                                                            </h3>
                                                        </div>
                                                        <div id="collapseThree" class="collapse" data-parent="#eventAboutAccordion">
                                                            <div class="card-body">
                                                                <p>{{$data->growth}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-header" id="headingThree">
                                                            <h3 data-toggle="collapse" data-target="#collapseThree">
                                                                Height
                                                                <span class="icons">
                                                            <i class="fa fa-plus"></i>
                                                            <i class="fa fa-minus"></i>
                                                        </span>
                                                            </h3>
                                                        </div>
                                                        <div id="collapseThree" class="collapse" data-parent="#eventAboutAccordion">
                                                            <div class="card-body">
                                                                <p>{{$data->height}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-header" id="headingThree">
                                                            <h3 data-toggle="collapse" data-target="#collapseThree">
                                                                Light Demand
                                                                <span class="icons">
                                                            <i class="fa fa-plus"></i>
                                                            <i class="fa fa-minus"></i>
                                                        </span>
                                                            </h3>
                                                        </div>
                                                        <div id="collapseThree" class="collapse" data-parent="#eventAboutAccordion">
                                                            <div class="card-body">
                                                                <p>{{$data->lightdemand}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-header" id="headingThree">
                                                            <h3 data-toggle="collapse" data-target="#collapseThree">
                                                                co
                                                                <span class="icons">
                                                            <i class="fa fa-plus"></i>
                                                            <i class="fa fa-minus"></i>
                                                        </span>
                                                            </h3>
                                                        </div>
                                                        <div id="collapseThree" class="collapse" data-parent="#eventAboutAccordion">
                                                            <div class="card-body">
                                                                <p>{{$data->co}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                             
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!-- End Product Info Area -->
            </div>
        </div>
    </div>

    <section class="related-products-area mt-120 mt-md-80 mt-sm-60 mb-120 mb-md-80 mb-sm-60">
        <div class="container">
            <div class="row">
            @if($datas->count() !==0)
                <div class="col-12">
                    <div class="area-title text-center mb-54 mb-sm-34">
                        <h2>Related Products.</h2>
                    </div>
                </div>

                <div class="col-12">
                    <div class="products-wrapper">
                        <div class="ht-slick-slider-wrapper">
                            <div class="ht-slick-slider slick-row-10"
                                 data-slick='{"slidesToShow":3, "slidesToScroll": 3, "dots": true, "responsive": [{"breakpoint": 481,"settings": {"slidesToShow":1,"slidesToScroll": 1}},{"breakpoint": 769,"settings": {"slidesToShow":2,"slidesToScroll": 2}}]}'>
                                @foreach($datas as $related)
                                <!-- Start Single Product Wrap -->
                                <div class="single-product-wrap">
                                    <figure class="product-thumb">
                                        <a href="/shop/{{$related->id}}">
                                            <img class="bg-cover-300" src="/image/gallery/{{$related->gallerys[0]->photo}}" alt="Product"/>
                                        </a>

                                        <figcaption class="product-hvr-content">
                                            <!-- <button class="btn-quick-view trio-tooltip" data-tippy-content="Quick View"
                                                    data-toggle="modal" data-target="#quick-view"><i
                                                    class="fa fa-search"></i>
                                            </button> -->
                                            <a href="/shop/{{$related->id}}" class="btn-add-cart">Details</a>
                                        </figcaption>
                                    </figure>

                                    <div class="product-details">
                                        <div class="d-flex justify-content-between">
                                            <h3 class="product-title"><a href="/shop/{{$related->id}}">{{$related->title}}</a></h3>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product Wrap -->
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            </div>
        </div>
    </section>
</div>
<!--== End Page Content Wrapper ==-->

@endsection