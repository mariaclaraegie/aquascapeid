@extends('layouts.front')
@section('content')
<!--== Start Page Header Area ==-->
<div class="page-header-wrapper bg-offwhite">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="page-header-content d-flex">
                    <h1>Products</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!--== End Page Header Area ==-->

<!--== Start Page Content Wrapper ==-->
<div class="page-wrapper">
    <div class="shop-page-content-wrapper fix mt-120 mt-md-80 mt-sm-60 mb-md-80 mb-sm-60">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="product-filter-area mb-30">
                        <div class="product-filter-item">
                            <p class="m-0">Showing {{$datas->count()}} results </p>
                        </div>
                        <div class="product-filter-item mt-sm-14">
                            <div class="product-sorting">
                                <select name="product-sort" id="mySelect"  onchange="myFunction()">
                                    <!-- <option value="sbd">Default Sorting</option>
                                    <option value="sbp">Short By Popularity</option>
                                    <option value="sbar">Short By Average Rating</option> -->
                                    
                                    <option value="title" {{ Request::path() == 'index/title' ? 'selected' : '' }}>Sort By Name</option>
                                    <option value="updated_at" {{ Request::path() == 'index/updated_at' ? 'selected' : '' }}>Sort By Latest</option>
                                </select>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="col-lg-9 order-0">
                    <div class="products-wrapper grid-layout">
                        <div class="row">
                        @foreach($datas as $data)
                            <div class="col-sm-6 col-lg-4">
                                <!-- Start Single Product Wrap -->
                                <div class="single-product-wrap">
                                    <figure class="product-thumb">
                                        <a href="/shop/{{$data->id}}"><img src="/image/gallery/{{$data->gallerys[0]->photo}}" class="bg-cover-300" alt="Product"/></a>
                                        <figcaption class="product-hvr-content">
                                            <button class="btn-quick-view trio-tooltip" data-tippy-content="Quick View"
                                                    data-toggle="modal" data-target="#quick-view"
                                                    data-mytitle="{{$data->title}}"
                                                    data-desc="{{str_limit($locale == 'id' ? $data->decs : $data->leveldesc ,160)}}"
                                                    data-category="{{$data->subcategory->category}}"
                                                    data-photo="/image/gallery/{{$data->gallerys[0]->photo}}">
                                                    <i class="fa fa-search"></i>
                                            </button> 
                                            <a href="/shop/{{$data->id}}" class="btn-add-cart">Details</a>
                                        </figcaption>
                                    </figure>

                                    <div class="product-details">
                                        <div class="d-flex justify-content-between">
                                            <h3 class="product-title"><a href="/shop/{{$data->id}}">{{$data->title}}</a></h3>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product Wrap -->
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 order-2 order-lg-1">
                    <aside class="sidebar-area-wrapper mt-md-80 mt-sm-60">
                        <!-- Start Single Sidebar Wrap -->
                        <!-- <div class="single-sidebar-item-wrap">
                            <div class="sidebar-body">
                                <div class="sidebar-search-wrap">
                                    <form action="{{ url('/searchproduct') }}" method="POST" enctype="multipart/form-data">>
                                    <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                        <div class="src-from-content d-flex">
                                            <input type="search" name="search" placeholder="Search..."/>
                                            <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> -->
                        <!-- End Single Sidebar Wrap -->

                        <!-- Start Single Sidebar Wrap -->
                        <!-- <div class="single-sidebar-item-wrap">
                            <h3 class="sidebar-title">Filter By Price</h3>
                            <div class="sidebar-body">
                                <div class="price-range-wrap">
                                    <div class="price-range" data-min="50" data-max="400"></div>
                                    <div class="range-slider">
                                        <form action="#">
                                            <div class="range-inner">
                                                <label for="amount">Price: </label>
                                                <input type="text" id="amount" disabled/>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- End Single Sidebar Wrap -->

                        <!-- Start Single Sidebar Wrap -->
                        <div class="single-sidebar-item-wrap">
                            <h3 class="sidebar-title">Product Categories</h3>
                            <div class="sidebar-body">
                                <ul class="sidebar-list">
                                @foreach($categorys as $category)
                                    <li><a href="{{ url('/category/'.$category->id) }}">{{$category->category}}</a></li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- End Single Sidebar Wrap -->
                    </aside>
                </div>

                <div class="col-12 order-1 order-lg-2">
                    <!--== Start Pagination Area ==-->
                    <div class="pagination-content bg-offwhite mt-118 mt-md-78 mt-sm-58">
                    {{$datas->links()}}
<!--                        
                         <ul class="nav justify-content-center">
                            <li class="btn-arrow btn-prev mr-auto"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#" class="active">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li class="btn-arrow btn-next ml-auto"><a href="#"><i class="fa fa-angle-right"></i></a>
                            </li>
                        </ul> -->
                    </div>
                    <!--== End Pagination Area ==-->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function myFunction() {
  var x = document.getElementById("mySelect").value;
  location.href='/index/'+x;
}
</script>
<!--== End Page Content Wrapper ==-->
@endsection