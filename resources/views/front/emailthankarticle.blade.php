<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">

    <title>Aquascape Indonesia</title>

    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon"/>

    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Open Sans:300,400,400i,500,600,700%7CPlayfair+Display:400,400i"
          rel="stylesheet">

    <!--=== All Plugins CSS ===-->
    <link href="{{ asset('assets/front-assets/css/plugins.css') }}" rel="stylesheet">
    <!--=== All Vendor CSS ===-->
    <link href="{{ asset('assets/front-assets/css/vendor.css') }}" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="{{ asset('assets/front-assets/css/style.css') }}" rel="stylesheet">

    <!-- Modernizer JS -->
    <script src="{{ asset('assets/js/modernizr-2.8.3.min.js') }}"></script>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="preloader-active">

<!--== Start PreLoader Wrap ==-->
<div class="preloader-area-wrap">
    <div class="spinner d-flex justify-content-center align-items-center h-100">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<!--== End PreLoader Wrap ==-->

<!--== Start Page Content Wrapper ==-->
<div class="page-wrapper coming-soon">
    <!-- Start Coming Soon Page Content -->
    <section class="coming-soon-page-wrapper bg-img" data-bg="https://i.pinimg.com/originals/1b/8a/d4/1b8ad41561a9795baa56f53af17c4c28.jpg">
        <div class="container-fluid p-0 h-100">
            <div class="row no-gutters h-100">
                <div class="col-lg-6 col-xl-6 h-100 ml-auto text-center">
                    <div class="coming-soon-content h-100">
                        <div class="coming-soon-content-inner">
                            <h2>Thank You</h2>
                            <p class="text-brand">FOR SENDING US SOME INFORMATION</p>
                            <!-- <p>We will reach you in 24 hours.</p> -->
                            <div class="newsletter-form-wrap layout--2 mt-44">
                                 <!-- Start Contact Info Area -->
                                <div class="contact-info-area mt-sm-50">
                                    <div class="contact-info-wrap text-center mt-50 mt-md-30">
                                        <p class="mb-30 mb-md-20 text-white">
                                            <strong>
                                                PT.  BUMI AQUA NUSANTARA <br>
                                                Cilandak Warehouse Commercial Area 410, Jalan Raya Cilandak KKO <br>
                                                Cilandak Timur, Pasar Minggu, Jakarta Selatan 12560 <br>
                                            </strong>
                                        </p>

                                        <p class="m-0 text-white">
                                            <strong>
                                                Phone: +62 21 786 7590 <br>
                                                Email: info@aquascape-indonesia.com<br/>
                                                website: <a style="color: #12a89e;" href="www.aquascape-indonesia.com">www.aquascape-indonesia.com</a>
                                            </strong>
                                        </p>
                                    </div>
                                </div>
                                <!-- End Contact Info Area -->
                            </div>
                        </div>
                        <div class="mt-114 mt-md-74 mt-sm-56 justify-content-center">
                            <a href="/home" class="btn btn-md btn-brands" style="background-color: #12a89e; text-align: center; font-weight: 700;color: white; padding: 5px 50px;">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Coming Soon Page Content -->
</div>
<!--== End Page Content Wrapper ==-->

<!--=======================Javascript============================-->
<!--=== All Vendor Js ===-->
<script src="{{ asset('assets/front-assets/js/vendor.js') }}"></script>
<!--=== All Plugins Js ===-->
<script src="{{ asset('assets/front-assets/js/plugins.js') }}"></script>
<!--=== Active Js ===-->
<script src="{{ asset('assets/front-assets/js/active.min.js') }}"></script>

<!--=== Revolution Slider Js ===-->
<script src="{{ asset('assets/front-assets/js/revslider/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/jquery.themepunch.revolution.min.js') }}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('assets/front-assets/js/revslider/extensions/revolution.extension.video.min.js') }}"></script>


<script src="{{ asset('assets/front-assets/js/revslider/revslider-active.js') }}"></script>
</body>
</html>