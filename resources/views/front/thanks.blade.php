@extends('layouts.about')

@section('content')
<!--== Start Page Header Area ==-->
<div class="page-header-wrapper bg-offwhite">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="page-header-content d-flex">
                    <h1>Thank you</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!--== End Page Header Area ==-->

<!--== Start Contact Content Area ==-->
    <section class="contact-content-area mt-120 mt-md-80 mt-sm-60 mb-120 mb-md-80 mb-sm-54">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="mb-8" style="font-weight: 700">Thank You For Contacting Us</h2>
                    <p class="m-0">We will contact you soon.</p>
                </div>
            </div>
        </div>
    </section>
    <!--== End Contact Content Area ==-->
</div>
<!--== End Page Content Wrapper ==-->
@endsection