@extends('layouts.front')

@section('content')

<!--== Start Page Content Wrapper ==-->
<div class="page-wrapper">
    <!--== Start Contact Page Hero Area ==-->
    <section class="about-hero-area fixed-height parallaxBg bg-img" data-bg="assets/img/contact/hero-bg-1.jpg">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-lg-7 text-center m-auto">
                    <div class="about-us-content white mt-100">
                        <h2>Post Your Article</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--== End Contact Page Hero Area ==-->

<!--== Start Contact Content Area ==-->
    <section class="contact-content-area mt-120 mt-md-80 mt-sm-60 mb-120 mb-md-80 mb-sm-54">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <!-- Start Contact Form Area -->
                    <div class="contact-form-area-wrapper">
                        <div class="area-title mb-44">
                            <h2 class="mb-8" style="font-weight: 700">Post Your Article</h2>
                            <p class="m-0">Let's write down your article in the above box and we will review it soon.</p>
                        </div>

                        <div class="contact-form-wrapper">
                           <form action="{{url('sendarticle')}}" method="post">
                             <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                <div class="form-input-item">
                                    <input type="text" name="name" placeholder="Title" required/>
                                </div>

                                <div class="form-input-item">
                                    <input type="email" name="email" placeholder="Your Email (Required)" required/>
                                </div>

                                <div class="form-input-item">
                                    <textarea name="messages" id="message" cols="80" rows="9" placeholder="Write Your Article Here..."></textarea>
                                </div>

                                <div class="form-input-item">
                                    <button type="submit" class="btn btn-md btn-brands" style="background-color: #12a89e; font-weight: 700;color: white">Send Article</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Contact Form Area -->
                </div>
            </div>
        </div>
    </section>
    <!--== End Contact Content Area ==-->
</div>
<!--== End Page Content Wrapper ==-->
@endsection