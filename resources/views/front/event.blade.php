@extends('layouts.front')

@section('content')
<style>
.read-more {
    font-weight: 600;
    font-size: 14px;
    text-transform: uppercase;
    color: #12a89e;
}
</style>
<!--== Start Page Header Area ==-->
<div class="page-header-wrapper bg-offwhite" style="padding-top: 100px;">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="page-header-content d-flex">
                    <h1>Event</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!--== End Page Header Area ==-->
<!--== Start Page Content Wrapper ==-->
<div class="page-wrapper">
    <div class="blog-page-content-wrapper fix mt-120 mt-md-80 mt-sm-60">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="blog-list-content mtm-44">
                    @if($datas->count() !==0)
                    @foreach($datas as $data)
                        <!-- Single Blog Post Start -->
                        <div class="single-blog-post-wrap layout--2 layout-blog-page list-view">
                            <div class="row">
                                <div class="col-md-5">
                                    <figure class="blog-thumbnail">
                                        <a href="{{ URL::to('events/' . $data->id) }}"><img class="bg-cover-500" src="/image/event/{{$data->photo}}"
                                                                         alt="Blog Thumb"/></a>
                                        <figcaption class="blog-hvr-btn">
                                            <span class="post-type"><i class="fa fa-quote-right"></i></span>
                                        </figcaption>
                                    </figure>
                                </div>

                                <div class="col-md-7 my-auto">
                                    <div class="blog-post-details">
                                        <h2><a href="{{ URL::to('events/' . $data->id) }}">{{$locale == 'id' ? $data->title : $data->judul}}</a></h2>
                                        <div class="post-meta">
                                            <!-- <a href="blog-details.html"><i class="fa fa-eye"></i> 320 Views</a> -->
                                        </div>
                                        <p>{!!str_limit($locale == 'id' ? $data->desc : $data->deskripsi, 200)!!}</p>
                                        <br/>
                                        <br/>
                                        <a href="{{ URL::to('events/' . $data->id) }}" class="btn-read-more">Read More <i class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Single Blog Post End -->
                    @endforeach
                    @else
                        <!-- Single Blog Post Start -->
                        <div class="single-blog-post-wrap layout--2 layout-blog-page list-view">
                            <div class="row">
                                <div class="col-md-12">
                                <h4 style="text-align: center;font-weight: 300px; color: #808285;padding-bottom: 120px;">{{$locale == 'id' ? 'Tidak ada Event Saat ini' : 'There is no Event available right now'}}</h4>
                                </div>
                            </div>
                        </div>
                        <!-- Single Blog Post End -->
                    @endif
                    </div>
                </div>

                <div class="col-12">
                    <!--== Start Pagination Area ==-->
                     
                    <!--<div class="pagination-content bg-offwhite mt-120 mt-md-80 mt-sm-56">
                         <ul class="nav justify-content-center">
                            <li class="btn-arrow btn-prev mr-auto"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#" class="active">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li class="btn-arrow btn-next ml-auto"><a href="#"><i class="fa fa-angle-right"></i></a>
                            </li> 
                        </ul>
                    </div>-->
                    <!--== End Pagination Area ==-->
                </div>
            </div>
        </div>
    </div>
</div>
<!--== End Page Content Wrapper ==-->
@endsection