@extends('layouts.front')

@section('content')


<style>
@media (max-width: 769px) {
    .mobile-slider {
        height: 250px;
    }
    .rev-mobile {
        height: 250px !important;
    }
    .tp-caption {
        font-size: 18px !important;
        line-height: 36px !important;
        min-width: 290px !important;
        max-width: 290px !important;
        top: -60px !important;
    }
    .tp-desc {
        font-size: 11px !important;
        line-height: 16px !important;
        min-width: 220px !important;
        max-width: 220px !important;
        margin-top: -157px !important;
    }
    .event-section {
        padding-top: 10px !important;
        font-size: 12px;
    }
    .pl-20 {
        padding-left: 20px;
    }
    .event-title {
        font-size: 17px !important;
    }
}
</style>
<!-- Start Hero Slider Area Wrapper -->
<div class="slider-area-wrapper fix mobile-slider" style="margin-top: 86px">
    <div id="rev_slider_corporate_wrapper" class="rev_slider_wrapper fullscreen-container rev-mobile" data-alias="classic-agency"
         data-source="gallery">
        <div id="rev_slider_corporate" class="rev_slider fullscreenbanner" data-version="5.4.7">
            <ul>
            @foreach($sliders as $slide)
                <li data-index="{{$slide->id}}" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                    data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default"
                    data-masterspeed="1000" data-rotate="0"
                    data-saveperformance="off" data-title="Slide">
                    <!-- MAIN IMAGE -->
                    <img src="/image/slide/{{$slide->photo}}" alt="" data-bgposition="center center"
                         data-bgfit="cover"
                         data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption   tp-resizeme" id="slide-36-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-93','-143','-143','-143']"
                         data-fontsize="['90','80','60','50']" data-lineheight="['90','80','60','50']"
                         data-width="['none','none','none','360']" data-height="none"
                         data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":810,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":800,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 5; white-space: nowrap; font-size: 90px; line-height: 90px; font-weight: 700; color: #ffffff;">
                         {{$locale == 'id' ? $slide->title : $slide->judul}}
                    </div>

                    <!-- LAYER NR. 9 -->
                    <div class="tp-caption tp-desc tp-resizeme" id="slide-36-layer-2"
                         data-x="['center','center','center','center']" data-hoffset="['-3','-3','-3','-1']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['10','10','10','8']"
                         data-fontsize="['35','35','35','37']" data-lineheight="['50','50','50','44']"
                         data-width="['594','594','594','333']" data-height="['51','51','51','none']"
                         data-whitespace="normal" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1160,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":800,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 6; min-width: 594px; max-width: 594px; max-width: 51px; max-width: 51px; white-space: normal; font-size: 16px; line-height: 30px; font-weight: 500; color: #ffffff;">
                         {{$locale == 'id' ? $slide->decs : $slide->deskripsi}}
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<!-- End Hero Slider Area Wrapper -->

<!-- Start Hero Slider Area Wrapper -->
<div class="slider-area-wrapper fix">
    <div id="rev_slider_corporate_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="classic-agency"
         data-source="gallery">
        <div id="rev_slider_corporate" class="rev_slider fullscreenbanner" data-version="5.4.7">
            <ul>
            @foreach($sliders as $slide)
                <li data-index="{{$slide->id}}" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                    data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default"
                    data-masterspeed="1000" data-rotate="0"
                    data-saveperformance="off" data-title="Slide">
                    <!-- MAIN IMAGE -->
                    <img src="/image/slide/{{$slide->photo}}" alt="" data-bgposition="center center"
                         data-bgfit="cover"
                         data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption   tp-resizeme" id="slide-36-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-93','-143','-143','-143']"
                         data-fontsize="['90','80','60','50']" data-lineheight="['90','80','60','50']"
                         data-width="['none','none','none','360']" data-height="none"
                         data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":810,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":800,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 5; white-space: nowrap; font-size: 90px; line-height: 90px; font-weight: 700; color: #ffffff;">
                         {{$locale == 'id' ? $slide->title : $slide->judul}}
                    </div>

                    <!-- LAYER NR. 9 -->
                    <div class="tp-caption   tp-resizeme" id="slide-36-layer-2"
                         data-x="['center','center','center','center']" data-hoffset="['-3','-3','-3','-1']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['10','10','10','8']"
                         data-fontsize="['35','35','35','37']" data-lineheight="['50','50','50','44']"
                         data-width="['594','594','594','333']" data-height="['51','51','51','none']"
                         data-whitespace="normal" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1160,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":800,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 6; min-width: 594px; max-width: 594px; max-width: 51px; max-width: 51px; white-space: normal; font-size: 16px; line-height: 30px; font-weight: 500; color: #ffffff;">
                         {{$locale == 'id' ? $slide->decs : $slide->deskripsi}}
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<!-- End Hero Slider Area Wrapper -->

<!--== Start About Me Area ==-->
@if($events->count() !==0)
<section class="bg-black pt-100 pb-100 event-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-me-content">
                    <div class="about-author-info">
                        <h2 class="white pl-20 event-title">Our Next Event</h2>
						<!--== Start Post Slider Wrapper ==-->
						<div class="slider-post-wrapper" style="padding-top: 50px;">
							<div class="container custom-width">
								<div class="row">
									<div class="col-12">
										<div class="post-slider-content-wrap">
											<div class="ht-slick-slider"
												 data-slick='{"slidesToShow": 2, "slidesToScroll": 1, "dots": true, "responsive": [{"breakpoint": 992,"settings":{"slidesToShow":1}}]}'>
												<!-- Single Post Slide Start -->
                                                @foreach($events as $event)
												<div class="single-post-slide-wrap bg-img" data-bg="/image/event/{{$event->photo}}">
													<div class="post-details">
														<div class="post-meta d-flex justify-content-between">
															<a href="#" class="cate-name"></a>
															<a href="#" class="comment"></a>
														</div>
														<div class="post-content">
															<a href="#" class="post-date">{{ \Carbon\Carbon::parse($event->updated_at)->format('d M Y')}}</a>
															<h2 class="event-title"><a href="{{ URL::to('events/' . $event->id) }}">{{$locale == 'id' ? $event->title : $event->judul}}</a></h2>
															<p>{!!str_limit($locale == 'id' ? $event->desc : $event->deskripsi,100)!!}</p>
															<a href="{{ URL::to('events/' . $event->id) }}" class="btn btn-bordered">Read More</a>
														</div>
													</div>
												</div>
                                                @endforeach
												<!-- Single Post Slide End -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--== End Post Slider Wrapper ==-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<!--== End About Me Area ==-->

<!--== Start Restaurant About Area ==-->
<section class="restaurant-about-area bg-ash pt-120 pt-md-80 pt-sm-60 pb-120 pb-md-80 pb-sm-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 order-1 my-auto">
                <!-- Start Restaurant About Content -->
                <div class="restaurant-about-content">
                    <h2>{{$locale == 'id' ? 'Tentang Kami' : 'Discover Our Story.'}}</h2>
                    <p>{{$locale == 'id'
                    ? 'PT Bumi Aqua Nusantara (BAN) didirikan pada akhir tahun 2018, di Jakarta, Indonesia. BAN dirintis dan dikelola oleh Albertus Indrakaryana (organisasi), Agung Siyono (desain & produksi), Wibhyanto D (pemasaran). Kantor Pusat kami berada di Jakarta Selatan.'
                    : 'PT Bumi Aqua Nusantara (BAN) was established in late 2018, in Jakarta, Indonesia. BAN is pioneered and managed by Albertus Indrakaryana (organization), Agung Siyono (design & production), Wibhyanto D (marketing). Our Headquarter is in South Jakarta.'}}
                    </p><br/>
                    <p>{{$locale == 'id'
                    ? 'Kami adalah spesialis dalam Desain Produk Hardscape dan Pengaturan Aquascape Air Tawar sebagai produk dan layanan utama kami. Kami juga menyediakan berbagai kayu dan batu Aquascape, peralatan Aquascape dan menyediakan layanan untuk mengembangkan Hardscape dan Aquascape dalam berbagai model dan dimensi.'
                    : 'We specialize in Hardscape Product Design and Freshwater Aquascape Setting as our main product and services. We also provide a variety of Aquascape wood and stone, Aquascape equipment and provide service to develop Hardscape and Aquascape in various models and dimensions.'}}
                    </p>
                </div>
                <a class="btn btn-md btn-brands" href="{{ url('/front/about') }}" style="background-color: #12a89e; font-weight: 700;color: white">{{$locale == 'id' ? 'Lebih Lanjut' : 'Discover More'}}</a>
                <!-- End Restaurant About Content -->
            </div>

            <div class="col-lg-7 order-0 order-lg-1">
                <!-- Start Restaurant About Gallery -->
                <div class="restaurant-about-gallery mb-sm-50 mb-md-50">
                    <div class="row masonry-grid">
                        <div class="col-sm-6">
                            <a href="#"><img style="background-size: cover; background-repeat: no-repeat; width: 100%; object-fit: cover; height: 200px;" src="http://aquascape-indonesia.com/image/gallery/9-SP%209000X450.jpg" alt="Restaurant"/></a>
                        </div>
                        <div class="col-sm-6 mt-xs-30">
                            <a href="#"><img style="background-size: cover; background-repeat: no-repeat; width: 100%; object-fit: cover; height: 430px;" src="https://i.pinimg.com/736x/92/50/86/9250863ac34a058b74c344948e0cad4e--aquarium-becken-aquarium-terrarium.jpg" alt="Restaurant"/></a>
                        </div>
                        <div class="col-sm-6 mt-30">
                            <a href="#"><img style="background-size: cover; background-repeat: no-repeat; width: 100%; object-fit: cover; height: 200px;" src="https://66.media.tumblr.com/2244b23a6719a048e20ce6c3eddd4db4/tumblr_ml86rjW0rj1s3c1oao1_500.jpg" alt="Restaurant"/></a>
                        </div>
                    </div>
                </div>
                <!-- End Restaurant About Gallery -->
            </div>
        </div>
    </div>
</section>
<!--== End Restaurant About Area ==-->

<!--== Start Page Content Wrapper ==-->
<div class="page-wrapper">
    <div class="blog-page-content-wrapper fix mt-120 mt-md-80 mt-sm-60">
        <div class="container">
            <div class="row">
                <h2 style="margin-left: 10px;" class="pt-50 pb-50">{{$locale == 'id' ? 'Artikel Terakhir' : 'Latest Article'}}</h2>
                <div class="col-12">
                    <div class="row mtm-44">
                    @foreach($blogs as $blog)
                        <div class="col-md-6 col-lg-4">
                            <!-- Single Blog Post Start -->
                            <div class="single-blog-post-wrap layout--2 layout-blog-page">
                                <figure class="blog-thumbnail">
                                    <a href="{{ URL::to('blog/' . $blog->id) }}"><img class="bg-cover-300" src="/image/article/{{$blog->photo}}"
                                                                     alt="Blog Thumb"/></a>
                                    <!-- <figcaption class="blog-hvr-btn"> 
                                         <a href="{{ URL::to('blog/' . $blog->id) }}" class="btn-plus"><img
                                                src="assets/front-assets/img/icons/plus.svg"
                                                alt="Icons"/></a> 
                                    </figcaption>-->
                                </figure>
                                <div class="blog-post-details">
                                    <h2><a href="{{ URL::to('blog/' . $blog->id) }}">{{$locale == 'id' ? $blog->judul : $blog->title}}</a></h2>
                                    <div class="post-meta">
                                        <!-- <a href="{{ URL::to('blog/' . $blog->id) }}"><i class="fa fa-eye"></i> 320 Views</a> -->
                                    </div>
                                    <p>{!!str_limit($locale == 'id' ? $blog->isi : $blog->article, 200)!!}</p>
                                    <a href="{{ URL::to('blog/' . $blog->id) }}" class="btn-read-more">{{$locale == 'id' ? 'Baca Lebih Lanjut' : 'Read More'}}
                                    <i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                            <!-- Single Blog Post End -->
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-3 bg-white"></div>
                <div class="col-6" style="text-align: center;">
                    <!--== Start Pagination Area ==-->
                    <div class="mt-114 mt-md-74 mt-sm-56 justify-content-center">
                        <a href="/blog" class="btn btn-md btn-brands" style="background-color: #12a89e; text-align: center; font-weight: 700;color: white; padding: 5px 50px;">{{$locale == 'id' ? 'Tampilkan Lebih Banyak' : 'Show More'}}</a>
                    </div>
                    <!--== End Pagination Area ==-->
                </div>
                <div class="col-3 bg-white"></div>
            </div>
        </div>
    </div>
</div>
<!--== End Page Content Wrapper ==-->

<!--== Start Category Product Area ==-->
<section class="category-product-area bg-offwhite mt-120 mt-md-88 mt-sm-70 pt-50">
        <div class="container-fluid">
            <div class="single-category-product">
                <div class="row" style="padding: 20px;">
                    <div class="col-lg-6 my-auto">
                        <div class="cate-product-banner-area">
                            <h2>{{$locale == 'id' ? 'Koleksi' : 'Hard Scape'}}<br> {{$locale == 'id' ? 'Hard Scape' : 'Collection'}}</h2>
                            <figure class="cate-thumb text-md-center">
                                <img class="bg-cover-shop" src="http://aquascape-indonesia.com/image/gallery/9-SP%209000X450.jpg" alt="Category Banner"/>
                                <figcaption class="banner-offer-text"><a href="{{ url('/category/3') }}" style="color: black;" class="btn-read-more">See all <i
                                    class="fa fa-angle-right"></i></a></figcaption>
                            </figure>
                            <a href="{{ url('/category/3') }}" class="banner-rotate-text">{{$locale == 'id' ? 'Katalog Produk' : 'Shop Collection'}}</a>
                        </div>
                    </div>
    
                    <div class="col-lg-6">
                        <!-- Start Products Wrapper -->
                        <div class="products-wrapper">
                            <div class="ht-slick-slider-wrapper fix-x">
                                <div class="ht-slick-slider slick-row-15"
                                     data-slick='{"slidesToShow": 2, "slidesToScroll": 2, "arrows": true, "prevArrow": ".prev-arrow", "nextArrow": ".next-arrow", "responsive": [{"breakpoint": 481,"settings": {"slidesToShow":1,"slidesToScroll": 1}},{"breakpoint": 992,"settings": {"slidesToShow":2,"slidesToScroll": 2}},{"breakpoint": 1199,"settings": {"slidesToShow":1,"slidesToScroll": 1}}]}'>
                                    @foreach($hardscapes as $hardscape)
                                    <!-- Start Single Product Wrap -->
                                    <div class="single-product-wrap">
                                        <figure class="product-thumb">
                                            <a href="/shop/{{$hardscape->id}}">
                                            <img class="bg-cover-shop-single" src="/image/gallery/{{$hardscape->gallerys[0]->photo}}" alt="Product"/></a>
    
                                            <figcaption class="product-hvr-content">
                                                <button class="btn-quick-view trio-tooltip" data-tippy-content="Quick View"
                                                    data-toggle="modal" data-target="#quick-view"
                                                    data-mytitle="{{$hardscape->title}}"
                                                    data-desc="{{str_limit($hardscape->decs,160)}}"
                                                    data-category="{{$hardscape->subcategory->category}}"
                                                    data-photo="/image/gallery/{{$hardscape->gallerys[0]->photo}}">
                                                    <i class="fa fa-search"></i>
                                                </button> 
                                                <!-- <a href="" class="btn-add-cart">Add to Cart</a> -->
                                            </figcaption>
                                        </figure>
    
                                        <div class="product-details">
                                            <div class="d-flex justify-content-between">
                                                <h3 class="product-title"><a href="/shop/{{$hardscape->id}}">{{$hardscape->title}}</a></h3>
                                                <!-- <a href="wishlist.html" class="btn-add-wishlist trio-tooltip"
                                                   data-tippy-content="Add to Wishlist" data-tippy-placement="left"><i
                                                        class="fa fa-heart-o"></i></a> -->
                                            </div>
                                            <!-- <div class="price-group">
                                                <span class="price">Rp{{number_format($hardscape->price,0)}}</span>
                                            </div> -->
                                        </div>
                                    </div>
                                    <!-- End Single Product Wrap -->
    
                                    @endforeach
                                </div>
    
                                <!-- Start Slick Navigation Icons -->
                                <div class="ht-slick-nav ht-slick-nav--three">
                                    <button class="prev-arrow"><i class="fa fa-caret-left"></i></button>
                                    <button class="next-arrow"><i class="fa fa-caret-right"></i></button>
                                </div>
                                <!-- End Slick Navigation Icons -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="single-category-product">
                <div class="row" style="padding: 50px;">
                    <div class="col-lg-6">
                        <!-- Start Products Wrapper -->
                        <div class="products-wrapper">
                            <div class="ht-slick-slider-wrapper fix-x">
                                <div class="ht-slick-slider slick-row-15"
                                     data-slick='{"slidesToShow": 2, "slidesToScroll": 2, "arrows": true, "prevArrow": ".prev-arrow-cat", "nextArrow": ".next-arrow-cat", "responsive": [{"breakpoint": 481,"settings": {"slidesToShow":1,"slidesToScroll": 1}},{"breakpoint": 992,"settings": {"slidesToShow":2,"slidesToScroll": 2}},{"breakpoint": 1199,"settings": {"slidesToShow":1,"slidesToScroll": 1}}]}'>
                                    <!-- Start Single Product Wrap -->
                                    @foreach($plants as $plant)
                                    <div class="single-product-wrap">
                                        <figure class="product-thumb">
                                            <a href="/shop/{{$plant->id}}">
                                            <img class="bg-cover-shop-single" id="photo" src="/image/gallery/{{$plant->gallerys[0]->photo}}"alt="Product"/>
                                            </a>
    
                                            <figcaption class="product-hvr-content">
                                                <button class="btn-quick-view trio-tooltip" data-tippy-content="Quick View"
                                                    data-toggle="modal" data-target="#quick-view"
                                                    data-mytitle="{{$plant->title}}"
                                                    data-desc="{{str_limit($plant->decs,160)}}"
                                                    data-category="{{$plant->subcategory->category}}"
                                                    data-photo="/image/gallery/{{$plant->gallerys[0]->photo}}">
                                                    <i class="fa fa-search"></i>
                                                </button> 
                                                <!-- <a href="" class="btn-add-cart">Add to Cart</a> -->
                                            </figcaption>
                                        </figure>
    
                                        <div class="product-details">
                                            <div class="d-flex justify-content-between">
                                                <h3 class="product-title" id="title{{$plant->id}}"><a href="/shop/{{$plant->id}}">{{$plant->title}}</a></h3>
                                                <!-- <a href="wishlist.html" class="btn-add-wishlist trio-tooltip"
                                                   data-tippy-content="Add to Wishlist" data-tippy-placement="left"><i
                                                        class="fa fa-heart-o"></i></a> -->
                                            </div>
                                            <!-- <div class="price-group">
                                                <span class="price">Rp{{number_format($plant->price,0)}}</span>
                                            </div> -->
                                        </div>
                                    </div>
                                    @endforeach
                                    <!-- End Single Product Wrap -->

                                </div>
    
                                <!-- Start Slick Navigation Icons -->
                                <div class="ht-slick-nav ht-slick-nav--three">
                                    <button class="prev-arrow-cat"><i class="fa fa-caret-left"></i></button>
                                    <button class="next-arrow-cat"><i class="fa fa-caret-right"></i></button>
                                </div>
                                <!-- End Slick Navigation Icons -->
                            </div>
                        </div>
                    </div>
    
                    <div class="col-lg-6 my-auto">
                        <div class="cate-product-banner-area">
                            <h2>Aquascape <br> Collection</h2>
                            <figure class="cate-thumb text-md-center">
                                <img class="bg-cover-shop" src="https://greenaqua2-greenaquallc.netdna-ssl.com/media/catalog/product/cache/3/image/1000x1000/9df78eab33525d08d6e5fb8d27136e95/3/0/30197-anubias-nana-pangolino-1_1.jpg" alt="Category Banner"/>
                                <figcaption class="banner-offer-text"><a href="{{ url('/category/1') }}" style="color: black;" class="btn-read-more">Get Your Collections <i
                                    class="fa fa-angle-right"></i></a></figcaption>
                            </figure>
                            <a href="{{ url('/category/1') }}" class="banner-rotate-text">{{$locale == 'id' ? 'Katalog Produk' : 'Shop Collection'}}</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-category-product">
                <div class="row" style="padding: 20px;">
                    <div class="col-lg-6 my-auto">
                        <div class="cate-product-banner-area">
                            <h2>Stone<br> Collection</h2>
                            <figure class="cate-thumb text-md-center">
                                <img class="bg-cover-shop" src="http://aquascape-indonesia.com/image/gallery/10-erangga%20Gray.jpg" alt="Category Banner"/>
                                <figcaption class="banner-offer-text"><a href="{{ url('/category/3') }}" style="color: black;" class="btn-read-more">See all <i
                                    class="fa fa-angle-right"></i></a></figcaption>
                            </figure>
                            <a href="{{ url('/category/3') }}" class="banner-rotate-text">{{$locale == 'id' ? 'Katalog Produk' : 'Shop Collection'}}</a>
                        </div>
                    </div>
    
                    <div class="col-lg-6">
                        <!-- Start Products Wrapper -->
                        <div class="products-wrapper">
                            <div class="ht-slick-slider-wrapper fix-x">
                                <div class="ht-slick-slider slick-row-15"
                                     data-slick='{"slidesToShow": 2, "slidesToScroll": 2, "arrows": true, "prevArrow": ".prev-arrow", "nextArrow": ".next-arrow", "responsive": [{"breakpoint": 481,"settings": {"slidesToShow":1,"slidesToScroll": 1}},{"breakpoint": 992,"settings": {"slidesToShow":2,"slidesToScroll": 2}},{"breakpoint": 1199,"settings": {"slidesToShow":1,"slidesToScroll": 1}}]}'>
                                    @foreach($stones as $stone)
                                    <!-- Start Single Product Wrap -->
                                    <div class="single-product-wrap">
                                        <figure class="product-thumb">
                                            <a href="/shop/{{$stone->id}}">
                                            <img class="bg-cover-shop-single" src="/image/gallery/{{$stone->gallerys[0]->photo}}" alt="Product"/></a>
    
                                            <figcaption class="product-hvr-content">
                                                <button class="btn-quick-view trio-tooltip" data-tippy-content="Quick View"
                                                    data-toggle="modal" data-target="#quick-view"
                                                    data-mytitle="{{$stone->title}}"
                                                    data-desc="{{str_limit($stone->decs,160)}}"
                                                    data-category="{{$stone->subcategory->category}}"
                                                    data-photo="/image/gallery/{{$stone->gallerys[0]->photo}}">
                                                    <i class="fa fa-search"></i>
                                                </button> 
                                                <!-- <a href="" class="btn-add-cart">Add to Cart</a> -->
                                            </figcaption>
                                        </figure>
    
                                        <div class="product-details">
                                            <div class="d-flex justify-content-between">
                                                <h3 class="product-title"><a href="/shop/{{$stone->id}}">{{$stone->title}}</a></h3>
                                                <!-- <a href="wishlist.html" class="btn-add-wishlist trio-tooltip"
                                                   data-tippy-content="Add to Wishlist" data-tippy-placement="left"><i
                                                        class="fa fa-heart-o"></i></a> -->
                                            </div>
                                            <!-- <div class="price-group">
                                                <span class="price">Rp{{number_format($hardscape->price,0)}}</span>
                                            </div> -->
                                        </div>
                                    </div>
                                    <!-- End Single Product Wrap -->
    
                                    @endforeach
                                </div>
    
                                <!-- Start Slick Navigation Icons -->
                                <div class="ht-slick-nav ht-slick-nav--three">
                                    <button class="prev-arrow"><i class="fa fa-caret-left"></i></button>
                                    <button class="next-arrow"><i class="fa fa-caret-right"></i></button>
                                </div>
                                <!-- End Slick Navigation Icons -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--== End Category Product Area ==-->

<!--== Start Testimonial Area Wrapper ==-->
<section class="testimonial-area parallaxBg bg-img" data-bg="assets/front-assets/img/slides-min.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 m-auto text-center">
            </div>
        </div>
    </div>
</section>
<!--== End Testimonial Area Wrapper ==-->

<!--== Start Page Content Wrapper ==-->
<div class="page-wrapper">

    <!--== Start Contact Content Area ==-->
    <section class="contact-content-area mt-120 mt-md-80 mt-sm-60 mb-120 mb-md-80 mb-sm-54">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-7">
                    <!-- Start Contact Form Area -->
                    <div class="contact-form-area-wrapper">
                        <div class="area-title mb-44">
                            <h2 class="mb-8" style="font-weight: 700">Get In Touch</h2>
                            <p class="m-0">{{$locale == 'id' ? 'Harap isi form dibawah untuk menghubungi kami' : "Let's write down your email in the above box to receive the useful
                                information."}}</p>
                        </div>

                        <div class="contact-form-wrapper">
                            <form action="{{url('send')}}" method="post">
                             <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                <div class="form-input-item">
                                    <input type="text" name="name" placeholder="Your Name (Required)" required/>
                                </div>

                                <div class="form-input-item">
                                    <input type="email" name="email" placeholder="Your Email (Required)" required/>
                                </div>

                                <div class="form-input-item">
                                    <textarea name="messages" id="message" cols="30" rows="9" name="message" placeholder="Your Message"></textarea>
                                </div>

                                <div class="form-input-item">
                                    <button type="submit" class="btn btn-md btn-brands" style="background-color: #12a89e; font-weight: 700;color: white">Send Message</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Contact Form Area -->
                </div>

                <div class="col-md-6 col-lg-5">
                    <!-- Start Contact Info Area -->
                    <div class="contact-info-area mt-sm-50">
                        <div class="contact-map-area">
                            <div id="map_content" class="h-100" data-lat="-6.300213" data-lng="106.814602"
                                    data-zoom="30"></div>
                        </div>

                        <div class="contact-info-wrap text-center mt-50 mt-md-30">
                            <p class="mb-30 mb-md-20">
                                <strong>
                                    PT.  BUMI AQUA NUSANTARA <br>
                                    Cilandak Warehouse Commercial Area 410, Jalan Raya Cilandak KKO <br>
                                    Cilandak Timur, Pasar Minggu, Jakarta Selatan 12560 <br>
                                </strong>
                            </p>

                            <p class="m-0">
                                <strong>
                                    Phone: +62 21 786 7590 <br>
                                    Email: info@aquascape-indonesia.com
                                    website: <a href="">www.aquascape-indonesia.com</a>
                                </strong>
                            </p>
                        </div>
                    </div>
                    <!-- End Contact Info Area -->
                </div>
            </div>
        </div>
    </section>
    <!--== End Contact Content Area ==-->
</div>
<!--== End Page Content Wrapper ==-->

<!--=== Start Quick View Content Wrapper ==-->
<div class="modal fade" id="quick-view">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="quick-view-content-wrap">
                    <div class="row">
                        <!-- Start Product Thumbnail Area -->
                        <div class="col-md-6">
                            <div class="product-thumb-area">
                                <div class="ht-slick-slider dots-style-three prod-thumb-inner"
                                     data-slick='{"slidesToShow": 1, "infinite": false, "dots": true}'>
                                    <figure class="port-details-thumb-item" style="background-size: cover; object-fit: cover;">
                                        <img id="myphoto" class="bg-cover bg-cover-thumb-pop" style="height: 460px;" src="" alt="product"/>
                                    </figure>
                                    <!-- <figure class="port-details-thumb-item">
                                        <img src="http://www.aquascapingworld.com/gallery/images/902/1_final_shot.jpg" alt="product"/>
                                    </figure>
                                    <figure class="port-details-thumb-item">
                                        <img src="assets/front-assets/img/home-landing-page/portfolio/03.jpg" alt="product"/>
                                    </figure> -->
                                </div>
                            </div>
                        </div>
                        <!-- End Product Thumbnail Area -->

                        <!-- Start Product Info Area -->
                        <div class="col-md-6">
                            <div class="product-details-info-content-wrap">
                                <div class="prod-details-info-content">
                                    <h2 id="h2"></h2>
                                    <!-- <div class="price-group">
                                        <span class="price"></span>
                                    </div> -->

                                    <p id="decs"></p>

                                    <div class="port-details-con-inner">
                                        <div class="single-post-details__footer m-0">
                                            <div class="single-post-details__footer__item">
                                                <div class="footer-item-left">
                                                    <h5 class="item-head"><i class="fa fa-tags"></i> Categories:</h5>
                                                </div>
                                                <div class="footer-item-right">
                                                    <ul class="cate-list nav">
                                                        <li><a href="#" id="category"></a></li>
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <!-- <button class="btn btn-sm btn-brands" style="background-color: #12a89e; font-weight: 700;color: white">Order Now</button> -->
                                </div>
                            </div>
                        </div>
                        <!-- End Product Info Area -->
                    </div>
                </div>
            </div>

            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
        </div>
    </div>
</div>
<!--=== End Quick View Content Wrapper ==-->

@endsection