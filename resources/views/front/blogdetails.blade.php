@extends('layouts.front')
@section('content')
<style>
@media screen and (min-width: 768px) {
    .image-main {
        max-height: 400px;
        margin-top: 30px;
        width: 100%;
        object-fit: cover;
    }
}
@media screen and (max-width: 768px) {
    .image-main {
        max-height: 135px;
        margin-top: 50px;
        width: 100%;
        object-fit: cover;
    }
}
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}
</style>
<!--== Start Page Content Wrapper ==-->
<div class="page-wrapper">
    <div class="blog-details-content-wrapper mt-120 mt-md-80 mt-sm-60 mb-120 mb-md-80 mb-sm-60">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Start Blog Post Details Content -->
                    <article class="single-post-details">
                        <header class="single-post-details__header">
                            <figure class="blog-post-thumb">
                                <img class="image-main" src="/image/article/{{$data->photo}}" alt="Blog Details"/>
                            </figure>

                            <div class="blog-post-head">
                                <h2>{{$data->judul}}</h2>

                                <div class="post-meta">
                                    <a href="#"><i class="fa fa-user-circle-o"></i> {{$data->user->name}}</a>
                                    <a href="#"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($data->created_at)->format('d M Y')}}</a>
                                </div>
                            </div>
                        </header>

                        <div class="single-post-details__body noselect">
                            <p class="noselect">{!!$data->isi!!}</p>
                        </div>

                        <footer class="single-post-details__footer">
                            <div class="single-post-details__footer__item" style="flex-basis: 100%; max-width: 100%;">
                                <div class="footer-item-left">
                                    <h5 class="item-head"><i class="fa fa-eye"></i> Viewed:</h5>
                                </div>
                                <div class="footer-item-right">
                                    <p>{{$data->readby}} kali</p>
                                </div>
                            </div>

                            <!-- <div class="single-post-details__footer__item">
                                <div class="footer-item-left">
                                    <h5 class="item-head"><i class="fa fa-share-alt"></i> Share:</h5>
                                </div>

                                <div class="footer-item-right">
                                    <div class="share-item">
                                        <a href="#" class="trio-tooltip" data-tippy-content="Share On Facebook"><i
                                                class="fa fa-facebook"></i></a>
                                        <a href="#" class="trio-tooltip" data-tippy-content="Share On Twitter"><i
                                                class="fa fa-twitter"></i></a>
                                    </div>
                                </div>
                            </div> -->
                        </footer>
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>
<!--== End Page Content Wrapper ==-->
@endsection