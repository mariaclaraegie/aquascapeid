@extends('layouts.front')

@section('content')
<!--== Start Page Header Area ==-->
<div class="page-header-wrapper bg-offwhite">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="page-header-content d-flex">
                    <h1>Contact Us</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!--== End Page Header Area ==-->

<!--== Start Contact Content Area ==-->
    <section class="contact-content-area mt-120 mt-md-80 mt-sm-60 mb-120 mb-md-80 mb-sm-54">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-7">
                    <!-- Start Contact Form Area -->
                    <div class="contact-form-area-wrapper">
                        <div class="area-title mb-44">
                            <h2 class="mb-8" style="font-weight: 700">Get In Touch</h2>
                            <p class="m-0">Let's write down your email in the above box to receive the useful
                                information.</p>
                        </div>

                        <div class="contact-form-wrapper">
                           <form action="{{url('send')}}" method="post">
                             <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                <div class="form-input-item">
                                    <input type="text" name="name" placeholder="Your Name (Required)" required/>
                                </div>

                                <div class="form-input-item">
                                    <input type="email" name="email" placeholder="Your Email (Required)" required/>
                                </div>

                                <div class="form-input-item">
                                    <textarea name="messages" id="message" cols="30" rows="9" placeholder="Your Message"></textarea>
                                </div>

                                <div class="form-input-item">
                                    <button type="submit" class="btn btn-md btn-brands" style="background-color: #12a89e; font-weight: 700;color: white">Send Message</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Contact Form Area -->
                </div>

                <div class="col-md-6 col-lg-5">
                    <!-- Start Contact Info Area -->
                    <div class="contact-info-area mt-sm-50">
                        <div class="contact-map-area">
                            <div id="map_content" class="h-100" data-lat="-6.300213" data-lng="106.814602"
                                    data-zoom="30"></div>
                        </div>

                        <div class="contact-info-wrap text-center mt-50 mt-md-30">
                            <p class="mb-30 mb-md-20">
                                <strong>
                                    PT.  BUMI AQUA NUSANTARA <br>
                                    Cilandak Warehouse Commercial Area 410, Jalan Raya Cilandak KKO <br>
                                    Cilandak Timur, Pasar Minggu, Jakarta Selatan 12560 <br>
                                </strong>
                            </p>

                            <p class="m-0">
                                <strong>
                                    Phone: +62 21 786 7590 <br>
                                    Email: info@aquascape-indonesia.com
                                    website: <a href="">www.aquascape-indonesia.com</a>
                                </strong>
                            </p>
                        </div>
                    </div>
                    <!-- End Contact Info Area -->
                </div>
            </div>
        </div>
    </section>
    <!--== End Contact Content Area ==-->
</div>
<!--== End Page Content Wrapper ==-->
@endsection