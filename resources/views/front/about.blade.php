@extends('layouts.about')

@section('content')
<style>
.avatars{
    width: 100px;
    height: 100px;
    object-fit: cover;
    border-radius: 50px;
}
@media only screen and (max-width: 986px) {
    .hidden-mobile {
        display: none;
    }
    .mobile-about {
        margin-top: 100px;
    }
}
</style>
<!--== Start Page Content Wrapper ==-->
<main class="page-wrapper">
    <section class="about-me-area-wrapper">
        <div class="container-fluid p-0">
            <div class="row">
                <!-- Start Author Profile Picture -->
                <div class="col-lg-3 order-0 order-lg-1 hidden-mobile author-profile-thumb bg-img"
                     data-bg="https://images.unsplash.com/photo-1533586359826-e559600588f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1548&q=80"></div>
                <!-- End Author Profile Picture -->
                <!-- Start Author Content Wrap -->
                <div class="col-lg-9 m-auto order-1">
                    <div class="author-con-wrapper pl-14 pr-14 w-75 w-sm-100 w-md-100 mx-auto">
                        <div class="author-con-inner mobile-about">
                            <h2>{{$locale == 'id' ? 'Tentang Kami' : 'About Us'}}</h2>
                            <br/>
                            <p>{{$locale == 'id'
                            ? 'PT Bumi Aqua Nusantara (BAN) didirikan pada akhir 2018, di Jakarta, Indonesia. BAN dirintis dan dikelola oleh Albertus Indrakaryana (organisasi), Agung Siyono (desain & produksi), Wibhyanto D (pemasaran). Kantor Pusat kami berada di Jakarta Selatan.'
                            : 'PT Bumi Aqua Nusantara (BAN) was established in late 2018, in Jakarta, Indonesia. BAN is pioneered and managed by Albertus Indrakaryana (organization), Agung Siyono (design & production), Wibhyanto D (marketing). Our Headquarter is in South Jakarta.'}}</p>
                            <p>{{$locale == 'id'
                            ? 'Kami adalah spesialis dalam Desain Produk Hardscape dan Pengaturan Aquascape Air Tawar sebagai produk dan layanan utama kami. Kami juga menyediakan berbagai kayu dan batu Aquascape, peralatan Aquascape dan menyediakan layanan untuk mengembangkan Hardscape dan Aquascape dalam berbagai model dan dimensi.'
                            : 'We specialize in Hardscape Product Design and Freshwater Aquascape Setting as our main product and services. We also provide a variety of Aquascape wood and stone, Aquascape equipment and provide service to develop Hardscape and Aquascape in various models and dimensions.'}}</p>
                            <br/>
                            <p><b>{{$locale == 'id' ? 'VISI KAMI' : 'OUR VISION'}}</b></p>
                            <p>{{$locale == 'id' ? 'Hardscape kami adalah pilihan Aquascape terbaik Anda' : 'Our Hardscape is your best choice of Aquascape'}}</p>
                            <p><b>{{$locale == 'id' ? 'MISI KAMI' : 'OUR MISSION'}}</b></p>
                            <p>{{$locale == 'id' ? 'Menjadikan Hardscape kami adalah pilihan Aquascape terbaik Anda' : 'Make our Hardscape as the best choice for your Aquascape'}}</p>
                            <p><b>SCOPE</b></p>
                            <p>{{$locale == 'id'
                            ? 'Produk seni Hardscape kami dikerjakan oleh tangan-tangan terampil. Perusahaan kami melayani pembuatan Hardscape dan mengirimkan dalam skala grosir sesuai dengan pesanan khusus, baik di dalam negeri maupun luar negeri.'
                            : 'Our Hardscape art products are done by skilled hands. Our company serves manufacturing Hardscape and sends on a wholesale scale according to special orders, both domestically and overseas.'}}
                            </p>
                            <p>{{$locale == 'id'
                            ? 'Kami merancang Aquascape dengan pesanan khusus, menjual dan menyewakan Aquascape ramah lingkungan yang berbasis ekologi, Gaya Belanda, Gaya Jepang, Gaya Indonesia, untuk keindahan kantor, hotel, kafe, apartemen, restoran.'
                            : 'We design Aquascape on special orders, sell and rent ecology-based environmentally friendly Aquascape, Dutch Style, Japan Style, Indonesian Style, for the beauty of your office, hotel, café, apartment, restaurant.'}}<p>
                            <p>{{$locale == 'id'
                            ? 'Kami juga melayani sebagai penyelenggara acara di bidang Aquascape, Aquascape Workshop, Aquascape Training, Aquascape Exhibition and Festival. Kami berpartisipasi dalam berbagai kompetisi Aquascape domestik dan mancanegara. Tujuan tim kami adalah sebagai sarana untuk dapat melayani kebutuhan Hardscape & Aquascape Anda.'
                            : 'We also serve event organizer services in the Aquascape field, Aquascape Workshop, Aquascape Training, Aquascape Exhibition and Festival. We participate in various domestic and foreign Aquascape competitions. The achievement of our team is our means to be able to serve the needs of your Hardscape & Aquascape.'}}</p>
                            <p>{{$locale == 'id'
                            ? 'Kami berharap menjadi bagian penting dari mereka yang mencintai lingkungan, dan dapat membagikan rasa cinta terhadap Aquascape kepada lebih banyak orang di seluruh dunia; baik sebagai pengamat, penghobi atau pengusaha Aquascape, seperti Anda.'
                            : 'We hope to be an important part of those who love the environment, share the love of the world of Aquascape to more people in the world; either as an observer, hobbyist or businessman Aquascape, like you.'}}</p>
                            <p>{{$locale == 'id'
                            ? 'Kami senang melayani kebutuhan Hardscape & Aquascape Anda. Hubungi kami untuk informasi lebih lanjut.'
                            : 'We are happy to serve the needs of your Hardscape & Aquascape. Contact us for more information.'}}</p>
                            <br/>
                            <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4" style="text-align: center">
                            <img class="avatars" src="/assets/indra.jpeg">
                            <p style="padding-top: 10px;">Albertus Indra Karyana</p>
                            <p class="text-blue" style="font-size: 14px">Director of PT. Bumi Aqua Nusantara</p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4" style="text-align: center">
                            <img class="avatars" src="/assets/wibi.png">
                            <p style="padding-top: 10px;">Dominicus Wibhyanto</p>
                            <p class="text-blue" style="font-size: 14px">Marketing of PT. Bumi Aqua Nusantara</p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4" style="text-align: center">
                            <img class="avatars" src="/assets/agung.png">
                            <p style="padding-top: 10px;">Agung Siyono</p>
                            <p class="text-blue" style="font-size: 14px">Production of PT. Bumi Aqua Nusantara</p>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Author Content Wrap -->
            </div>
        </div>
    </section>
</main>
<!--== End Page Content Wrapper ==-->
@endsection