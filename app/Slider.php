<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    //
    protected $table = 'sliders';
    protected $fillable = [
        'number',
        'photo',
        'status',
        'title',
        'decs',
        'judul',
        'deskripsi'
    ];
}
