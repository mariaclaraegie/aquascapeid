<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    //
    protected $table = 'abouts';
    protected $fillable = [
        'link',
        'title1',
        'desc1',
        'title2',
        'desc2',
        'title3',
        'desc3',
        'created_by'
    ];
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
