<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $table = 'articles';
    protected $fillable = [
        'number',
        'title',
        'article',
        'judul',
        'isi',
        'photo',
        'approved',
        'status', // 0 = draft, 1 = published not approved, 2 = publish approved
        'user_id',
        'readby'
    ];
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
