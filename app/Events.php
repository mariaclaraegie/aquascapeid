<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    //
    protected $table = 'events';
    protected $fillable = [
        'number',
        'title',
        'desc',
        'photo',
        'status',
        'judul',
        'deskripsi',
        'datelive',
        'dateend'
    ];
}
