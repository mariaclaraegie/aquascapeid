<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'categorys';
    protected $fillable = [
        'category',
        'status'
    ];
    public function subcategorys()
    {
        return $this->hasMany('App\SubCategory', 'category_id', 'id');
    }

}
