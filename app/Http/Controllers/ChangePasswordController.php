<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Hash;

class ChangePasswordController extends Controller
{
    //
     public function __construct()
    {
        $this->middleware('auth');
    }

    //Change Password View
    public function showChangePasswordForm(){
        return view('auth.passwords.change');
    }
   
   //Change Password Function
    public function changePassword(Request $request){
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = Validator::make($request->all(),[
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        if($validatedData->fails())
        {
            return redirect()->back()->withErrors($validatedData->errors());
        }
        else
        {
            $user = Auth::user();
            $user->password = bcrypt($request->get('new-password'));
            $user->save();
            return redirect()->back()->with("success","Password changed successfully !");
        }
    }
}
