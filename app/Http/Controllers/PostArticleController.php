<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Mail;

class PostArticleController extends Controller
{
    //
    public function index()
    {
        $categorys = Category::all()->where('status',1);
        return view('front.bloguser')->with('categorys',$categorys);
    }
    public function send(Request $request)
    {
        $this->validate($request, array('email' => 'required|email'));
        $data = array(
            'name' => $request->name,
            'bodyMessage' => $request->messages,
            'email' => $request->email,
            );
        Mail::send('mail' , $data , function($message) use ($data){
            $message->from($data['email']);
            $message->to('Info@aquascape-indonesia.com');
             $message->subject('Article From : ' . $data['name'] . '');
        });
        return view('front.emailthankarticle');
    }

}
