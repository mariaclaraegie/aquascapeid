<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Product;
use App\Category;
use App\Gallery;
use App\Repositories\Repository;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;
    public function __construct(Product $product)
    {
        $this->middleware('auth');
        $this->model = new Repository($product);
    }
    public function index()
    {
        // 
        if(Auth::user()->role == 1)
        {
            return redirect('home');
        }
        else
        {
        $datas = $this->model->all();
        $datas = $datas->where('ishide',0)->where('is_hidden',0);
        return view('cms.product.indexProduct')->with('datas',$datas);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categorys = Category::all();
        return view('cms.product.createProduct')
        ->with('categorys',$categorys);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $this->model->create($request->all());
        $item = strtoupper($data->subcategory->category) .'-0'.$data->id;
        $data->item_no =  $item;
        $data->price = preg_replace('/[^0-9]/','',$request->price);
        $data->save();
        

        if(file_exists($request->file('photo')))
        {
            $image = $request->file('photo');
            $new_name = $data->id."-" .$image->getClientOriginalName();
            //$image->move(public_path("image/gallery",$new_name);
            $image->move("image/gallery",$new_name);

            $gallery = new Gallery([
                'product_id' => $data->id,
                'photo'=>$new_name
            ]);
            $gallery->save();

            $data->complete =  1;
            $data->save();
        }
        return redirect()->route('product.index')->with('success','Product created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = $this->model->show($id);
        // return view('cms.product.createProduct')
        // ->with('data',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categorys = Category::all();
        $data = $this->model->show($id);
        return view('cms.product.editProduct')
        ->with('data',$data)
        ->with('categorys',$categorys);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $this->model->update($request->all(),$id);
        $data = $this->model->show($id);
        $data->price = preg_replace('/[^0-9]/','',$request->price);
        $data->save();
        return redirect()->route('product.index')->with('success','Product updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = $this->model->show($id);
        $data->ishide = 1;
        $data->save();
        return redirect()->route('product.index')->with('success','Product deleted successfully.');


    }
}
