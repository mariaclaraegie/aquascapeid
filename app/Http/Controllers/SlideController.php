<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Slider;
use App\Repositories\Repository;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;
    public function __construct(Slider $slide)
    {
        $this->middleware('auth');
        $this->model = new Repository($slide);
    }
    public function index()
    {
        //
        if(Auth::user()->role == 1)
        {
            return redirect('home');
        }
        else
        {
        $datas = $this->model->all();
        return view('cms.slider.slider')->with('datas',$datas);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if($request->file('photo1'))
        {
            $image = $request->file('photo1');
            $new_name = rand() . "-" .$image->getClientOriginalName();
            $image->move("image/slide",$new_name);
            $data = $this->model->show(1);
            $data->photo = $new_name ;
            $data->status = 1 ;
            $data->save();
        }
        if($request->file('photo2'))
        {
            $image = $request->file('photo2');
            $new_name = rand() . "-" .$image->getClientOriginalName();
            $image->move("image/slide",$new_name);
            $data = $this->model->show(2);
            $data->photo = $new_name ;
            $data->status = 1 ;
            $data->save();
        }
        if($request->file('photo3'))
        {
            $image = $request->file('photo3');
            $new_name = rand() . "-" .$image->getClientOriginalName();
            $image->move("image/slide",$new_name);
            $data = $this->model->show(3);
            $data->photo = $new_name ;
            $data->status = 1 ;
            $data->save();
        }
        if($request->file('photo4'))
        {
            $image = $request->file('photo4');
            $new_name = rand() . "-" .$image->getClientOriginalName();
            $image->move("image/slide",$new_name);
            $data = $this->model->show(4);
            $data->photo = $new_name ;
            $data->status = 1 ;
            $data->save();
        }
        if($request->file('photo5'))
        {
            $image = $request->file('photo5');
            $new_name = rand() . "-" .$image->getClientOriginalName();
            $image->move("image/slide",$new_name);
            $data = $this->model->show(5);
            $data->photo = $new_name ;
            $data->save();
        }
        if($request->file('photo6'))
        {
            $image = $request->file('photo6');
            $new_name = rand() . "-" .$image->getClientOriginalName();
            $image->move("image/slide",$new_name);
            $data = $this->model->show(6);
            $data->photo = $new_name ;
            $data->save();
        }
        return redirect()->route('slide.index')->with('success','Photo updated successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = $this->model->show($id);
        return view('cms.slider.editslide')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $this->model->show($id);
        $data->title = $request->title;
        $data->status = 1;
        $data->judul = $request->judul;
        $data->deskripsi = $request->deskripsi;
        $data->decs = $request->decs;
        if($request->file('photo'))
        {
            $image = $request->file('photo');
            $new_name = rand() . "-" .$image->getClientOriginalName();
            $image->move("image/slide",$new_name);
            $data->photo = $new_name ;
        }
        $data->save();
        return redirect()->route('slide.index')->with('success','Slide updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
    }
}
