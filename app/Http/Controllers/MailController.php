<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;


class MailController extends Controller
{
    //
    public function send(Request $request)
    {
        $this->validate($request, array('email' => 'required|email'));
        $data = array(
            'name' => $request->name,
            'bodyMessage' => $request->messages,
            'email' => $request->email,
            );
        Mail::send('mail' , $data , function($message) use ($data){
            $message->from($data['email']);
            $message->to('xiaa.mariaclaraegie@gmail.com');
             $message->subject('Contact from Website From : ' . $data['name'] . '');
        });
        return view('front.emailthanks');
    }
}
