<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Events;
use App\Repositories\Repository;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;
    public function __construct(Events $events)
    {
        $this->middleware('auth');
        $this->model = new Repository($events);
    }
    public function index()
    {
        //
        if(Auth::user()->role == 1)
        {
            return redirect('home');
        }
        else
        {
            $datas = $this->model->all();
            return view('cms.event.event')->with('datas',$datas);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = $this->model->show($id);
        return view('cms.event.editevent')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $this->model->show($id);
        $data->title = $request->title;
        $data->status = 1;
        $data->desc = $request->desc;
        $data->deskripsi = $request->deskripsi;
        $data->judul = $request->judul;
        if($request->file('photo'))
        {
            $image = $request->file('photo');
            $new_name = rand() . "-" .$image->getClientOriginalName();
            $image->move("image/event",$new_name);
            $data->photo = $new_name ;
        }
        $data->save();
        return redirect()->route('event.index')->with('success','Event updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //dd($id);
        $data = $this->model->show($id);
        $data->photo = 'default.png';
        $data->title = 'Event ' .$id;
        $data->desc = 'Event ' .$id;
        $data->deskrips = '';
        $data->judul = '';
        $data->status = 0;
        $data->save();
        return redirect()->route('event.index')->with('success','Event deleted successfully.');
    }
}
