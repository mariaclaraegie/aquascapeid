<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Category;
use App\Article;
use App\Repositories\Repository;
use App;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;
    public function __construct(Article $slide)
    {

        $this->model = new Repository($slide);
    }
    public function index()
    {
        //
        $datas = Article::where('approved',1)->where('status',1)->paginate(3);
        $locale = App::getLocale();
        $categorys = Category::all()->where('status',1);
        return view('front.blog')->with('datas',$datas)->with('categorys',$categorys)->with('locale',$locale);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = $this->model->show($id);
         $locale = App::getLocale();
        $data->readby +=1;
        $data->save();
        $categorys = Category::all()->where('status',1);
        return view('front.blogdetails')->with('data',$data)->with('categorys',$categorys)->with('locale',$locale);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $this->model->show($id);
        if($data->approved == 1)
        {
            $data->approved = 0;
        }
        else if($data->status == 1)
        {
            $data->approved = 1;
        }
        $data->save();
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
