<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Events;
use App\Product;
use App\Slider;
use App;
use App\Category;
use App\Article;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($params = "front")
    {
        //
        
        $locale = App::getLocale();

        $events = Events::all()->where('status',1);
        $categorys = Category::all()->where('status',1);
        $sliders = Slider::all()->where('status','1');
        $blogs = Article::all()->where('approved',1)->where('status',1)->random(3);

        $plants = Product::where('subcategory_id','1')
        ->where('ishide',0)
        ->where('is_hidden',0)
        ->where('complete',1)->orderByRaw('RAND()')->take(4)->get();

        $hardscapes = Product::where('subcategory_id','2')
        ->where('ishide',0)
        ->where('is_hidden',0)
        ->where('complete',1)->orderByRaw('RAND()')->take(4)->get();

        $stones = Product::where('subcategory_id','3')
        ->where('ishide',0)
        ->where('is_hidden',0)
        ->where('complete',1)->orderByRaw('RAND()')->take(4)->get();

        return view('front.' . $params)
        ->with('events',$events)
        ->with('plants',$plants)
        ->with('hardscapes',$hardscapes)
        ->with('blogs',$blogs)
        ->with('sliders',$sliders)
        ->with('locale',$locale)
        ->with('categorys',$categorys)
        ->with('stones',$stones);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
