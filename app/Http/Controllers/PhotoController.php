<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Gallery;
use App\Category;
use App\Repositories\Repository;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        protected $model;
    public function __construct(Gallery $gallery)
    {
        $this->middleware('auth');
        $this->model = new Repository($gallery);
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $this->model->create($request->all());
        $image = $request->file('photo');
        $new_name = $data->id."-" .$image->getClientOriginalName();
        //$image->move(public_path("image/gallery",$new_name);
        $image->move("image/gallery",$new_name);
        $data->photo = $new_name ;
        $data->save();

        $header = Product::find($data->product_id);
        $header->complete = 1;
        $header->save();
        return redirect()->route('photo.edit',$data->product_id)->with('success','Photo uploaded successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Product::find($id);
        return view('cms.product.photoProduct')->with('data',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = $this->model->show($id);
        $product_id = $data->product_id;
       // dd($data->id);
        $this->model->delete($id);

        $product = Product::find($product_id);
        if(count($product->gallerys) == 0)
        {
            $product->complete = 0;
            $product->save();
        }
        
        return redirect()->route('photo.edit',$product_id)->with('success','Photo deleted successfully.');

    }
}
