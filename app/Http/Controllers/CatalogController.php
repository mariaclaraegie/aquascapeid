<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App;
use App\Product;
use App\Category;
use App\Repositories\Repository;

class CatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;
    public function __construct(Product $product)
    {
        $this->model = new Repository($product);
    }
    public function index($params = "")
    {
        //
        $locale = App::getLocale();
        $categorys = Category::all()->where('status',1);

        if($params == "asc")
        {
           $datas = Product::where('ishide',0)->where('complete',1)->orderByDesc('price')->paginate(18);
        }
        else if($params != "")
        {
            $datas = Product::where('ishide',0)->where('complete',1)->orderBy($params)->paginate(18);
        }
        else
        {
            $datas = Product::where('ishide',0)->where('complete',1)->orderBy('title')->paginate(18);
        }

        return view('front.shop')->with('datas',$datas)
        ->with('categorys',$categorys)->with('locale',$locale);
    }

    public function subcategory($params = "")
    {
        //
        $locale = App::getLocale();
        $categorys = Category::all()->where('status',1)->sortBy('title');

       
        if($params != "")
        {
            $datas = Product::where('subcategory_id',$params)->orderBy('title')->paginate(18);
        }
        else
        {
             $datas = Product::where('ishide',0)->where('complete',1)->orderBy('title')->paginate(18);
        }
        
        return view('front.shop')->with('datas',$datas)
        ->with('categorys',$categorys)
        ->with('locale',$locale);
    }

    public function category($params = "")
    {
        //
        $locale = App::getLocale();
        $categorys = Category::all()->where('status',1);
        $datas = Product::where('subcategory_id', $params)->where('ishide',0)->where('complete',1)->paginate(3);
        return view('front.shop')->with('datas',$datas)
        ->with('categorys',$categorys)
        ->with('locale',$locale);
    }

    public function searchproduct(Request $request)
    {
        //
        
        $categorys = Category::all()->where('status',1);
        $search = $request->search;
        $datas = Product::where('title','LIKE',"%{$search}%")
              ->get();

        return view('front.shop')->with('datas',$datas)
        ->with('categorys',$categorys);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $locale = App::getLocale();
        $data = $this->model->show($id);
        $categorys = Category::all()->where('status',1);
        $datas = $this->model->all();
        $datas = $datas->where('subcategory_id',$data->subcategory_id)
        ->where('id','!=',$id)
        ->where('ishide',0)
        ->where('is_hidden',0)
        ->where('complete',1);
        
        return view('front.shopdetails')
        ->with('data',$data)
        ->with('datas',$datas)
        ->with('categorys',$categorys)
        ->with('locale',$locale);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
