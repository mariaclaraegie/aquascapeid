<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App;
use App\Category;
use App\Events;
use App\Repositories\Repository;

class EventFrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $model;
    public function __construct(Events $event)
    {
        $this->model = new Repository($event);
    }
    public function index()
    {
        //
        $datas = $this->model->all();
        $locale = App::getLocale();
        $datas = $datas->where('status',1);
        $categorys = Category::all()->where('status',1);
        return view('front.event')->with('datas',$datas)->with('categorys',$categorys)->with('locale',$locale);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = $this->model->show($id);
        $locale = App::getLocale();
        $categorys = Category::all()->where('status',1);
        return view('front.eventdetails')->with('data',$data)->with('categorys',$categorys)->with('locale',$locale);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
