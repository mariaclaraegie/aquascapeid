<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Article;
use App\Repositories\Repository;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;
    public function __construct(Article $slide)
    {
        $this->middleware('auth');
        $this->model = new Repository($slide);
    }
    public function index()
    {
        //
        if(Auth::user()->role == 1)
        {
            return redirect('home');
        }
        else
        {
        $datas = $this->model->all();
        return view('cms.article.listArticle')->with('datas',$datas);
        }
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('cms.article.createArticle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $this->model->create($request->all());;
        if($request->file('photo'))
        {
            $image = $request->file('photo');
            $new_name = $data->id."-" .$image->getClientOriginalName();
            //$image->move(public_path("image/article"),$new_name);
            $image->move("image/article",$new_name);
            $data->photo = $new_name ;
            $data->save();
        }
        else
        {
            $data->photo = 'default.png' ;
            $data->save();
        }
        return redirect()->route('article.index')->with('success','Article added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = $this->model->show($id);
        
        return view('cms.article.showArticle')->with('data',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = $this->model->show($id);
        return view('cms.article.editArticle')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->model->update($request->all(),$id);
        if($request->file('photo'))
        {
            $data = $this->model->show($id);
            $image = $request->file('photo');
            $new_name = $data->id."-" .$image->getClientOriginalName();
           // $image->move(public_path("image/article"),$new_name);
            $image->move("image/article",$new_name);
            $data->photo = $new_name ;
            $data->save();
        }
        return redirect()->route('article.index')->with('success','Article updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->model->delete($id);
        return redirect()->route('article.index')->with('success','Article deleted successfully.');

    }
}
