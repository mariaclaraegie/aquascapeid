<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    //
    protected $table = 'subcategorys';
    protected $fillable = [
        'category_id',
        'category',
        'status'
    ];
    public function categorys()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }
}
