<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';
    protected $fillable = [
        'subcategory_id',
        'title',
        'decs',
        'level',
        'leveldesc',
        'price',
        'type',
        'origin',
        'growth',
        'height',
        'lightdemand',
        'co',
        'item_no',
        'complete',
        'is_hidden'
    ];
    public function subcategory()
    {
        return $this->hasOne('App\Category', 'id', 'subcategory_id');
    }
    public function gallerys()
    {
        return $this->hasMany('App\Gallery', 'product_id', 'id');
    }
}
