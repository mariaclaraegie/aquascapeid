<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//multi language
Route::get('locale', function () {
    return \App::getLocale();
});

Route::get('/locale/{locale}', function ($locale) {
    \Session::put('locale', $locale);
    return redirect()->back();
});
//
Route::get('/about-cms/section-one', function () {
    return view('cms.about.sectionone');
});
Route::get('/about-cms/section-two', function () {
    return view('cms.about.sectiontwo');
});
Route::get('/about-cms', function () {
    return view('cms.about.about');
});
Route::get('/thankyou', function () {
    return view('front.thanks');
});

Route::get('/', function () {
    // return view('front.comingsoon');
    
    return redirect('home');
});

//front
Route::get('front/{home}', 'FrontController@index');
Route::get('about', 'AboutController@index');
Route::get('home/{home}', 'FrontController@index');
Route::get('/subcategory/{home}', 'CatalogController@subcategory');
Route::get('/category/{home}', 'CatalogController@category');
Route::get('/index/{title}', 'CatalogController@index');
//Route::post('/searchproduct', 'CatalogController@searchproduct');
Route::resource('home','FrontController');
Route::resource('blog','BlogController');
Route::resource('events','EventFrontController');
Route::resource('shop','CatalogController');
Route::resource('catalog','ProductCatalogController');

Route::get('/thanks', function () {
    return view('front.emailthanks');
});
Route::post('send','MailController@send');

Route::get('blog-user', 'PostArticleController@index');
Route::post('sendarticle','PostArticleController@send');
//auth
Auth::routes();
Route::get('/changePassword','ChangePasswordController@showChangePasswordForm');
Route::post('/changePassword','ChangePasswordController@changePassword')->name('changePassword');

//cms
Route::resource('slide','SlideController');
Route::resource('article','ArticleController');
Route::resource('event','EventController');
Route::resource('product','ProductController');
Route::resource('photo','PhotoController');
Route::get('/getsubcategory/{test}','HomeController@getsubcategory');
Route::get('/cms', function () {
    if(Auth::user()->role == 1)
    {
        return redirect('home');
    }
    else
    {
        return view('cms.dashboard');
    }
})->middleware('auth');
//

